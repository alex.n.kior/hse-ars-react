"use strict";
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable("Users", {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER,
      },
      email: {
        type: Sequelize.STRING,
      },
      password: {
        type: Sequelize.STRING,
      },
      is_admin: {
        type: Sequelize.BOOLEAN,
      },
      current_place_id: {
        type: Sequelize.INTEGER,
      },
      has_accomodation: {
        type: Sequelize.BOOLEAN,
      },
      is_approved: {
        type: Sequelize.BOOLEAN,
      },
      chosen_address_id: {
        type: Sequelize.INTEGER,
      },
      chosen_location_id: {
        type: Sequelize.INTEGER,
      },
      chosen_place_id: {
        type: Sequelize.INTEGER,
      },
      full_name: {
        type: Sequelize.STRING,
      },
      sex: {
        type: Sequelize.STRING,
      },
      date_of_birth: {
        type: Sequelize.STRING,
      },
      age: {
        type: Sequelize.INTEGER,
      },
      citizenship: {
        type: Sequelize.STRING,
      },
      documents: {
        type: Sequelize.STRING,
      },
      registration: {
        type: Sequelize.STRING,
      },
      phone: {
        type: Sequelize.STRING,
      },
      is_contract: {
        type: Sequelize.BOOLEAN,
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable("Users");
  },
};
