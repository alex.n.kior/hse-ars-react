'use strict';
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('Places', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      pic: {
        type: Sequelize.STRING
      },
      address_id: {
        type: Sequelize.INTEGER
      },
      location_id: {
        type: Sequelize.INTEGER
      },
      place_type: {
        type: Sequelize.STRING
      },
      sex: {
        type: Sequelize.STRING
      },
      cost_per_month: {
        type: Sequelize.INTEGER
      },
      deposit: {
        type: Sequelize.INTEGER
      },
      address: {
        type: Sequelize.STRING
      },
      rooms: {
        type: Sequelize.INTEGER
      },
      occupied: {
        type: Sequelize.INTEGER
      },
      unoccupied_in_room: {
        type: Sequelize.INTEGER
      },
      description: {
        type: Sequelize.STRING
      },
      bed_type: {
        type: Sequelize.STRING
      },
      bed_place: {
        type: Sequelize.STRING
      },
      place_plan: {
        type: Sequelize.STRING
      },
      floor_plan: {
        type: Sequelize.STRING
      },
      starts: {
        type: Sequelize.STRING
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('Places');
  }
};
