'use strict';

module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.bulkInsert(
            'Locations',
            [
                {
                    address_id: 1,
                    pic: 'https://www.hse.ru/mirror/pubs/share/208035605',
                    name: 'ВШЭ на Мясницкой',
                    address: 'г.Москва, Мясницкая',
                    unoccupied: 20,
                    price_min: 1000,
                    price_max: 10000,
                    description: 'нормас ваще, размещайся',
                    createdAt: new Date(),
                    updatedAt: new Date(),
                },
                {
                    address_id: 1,
                    pic: 'https://www.hse.ru/mirror/pubs/share/208035605',
                    name: 'ВШЭ на Мясницкой',
                    address: 'г.Москва, Мясницкая',
                    unoccupied: 300,
                    price_min: 10000,
                    price_max: 1000000,
                    description: 'еше луче, размещайся',
                    createdAt: new Date(),
                    updatedAt: new Date(),
                },
                {
                    address_id: 2,
                    pic: 'https://www.hse.ru/data/2019/08/02/1484622351/20190523_4293.jpg',
                    name: 'ВШЭ на Бульваре',
                    address: 'г.Москва, Бульвар',
                    unoccupied: 100,
                    price_min: 10000,
                    price_max: 1000000,
                    description: 'еше луче, размещайся',
                    createdAt: new Date(),
                    updatedAt: new Date(),
                },
            ],
            {},
        );
    },

    down: (queryInterface, Sequelize) => {
        return queryInterface.bulkDelete('Locations', null, {});
    },
};
