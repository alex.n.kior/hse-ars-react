'use strict';

module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.bulkInsert(
            'Addresses',
            [
                {
                    pic: 'https://telekomza.ru/static/providers/61/logo/New_logotype_Dom_1.png?1637240806',
                    name: 'Дом ру',
                    unoccupied: 153,
                    description: 'нормас ваще, размещайся',
                    createdAt: new Date(),
                    updatedAt: new Date(),
                },
                {
                    pic: 'https://www.artlebedev.com/cian/logo2/cian-logo2-site.jpg',
                    name: 'Циан',
                    unoccupied: 11000,
                    description: 'еше луче, размещайся',
                    createdAt: new Date(),
                    updatedAt: new Date(),
                },
            ],
            {},
        );
    },

    down: (queryInterface, Sequelize) => {
        return queryInterface.bulkDelete('Addresses', null, {});
    },
};
