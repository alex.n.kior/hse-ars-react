"use strict";

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert(
      "Users",
      [
        {
          email: "test@hse.ars",
          password: "aadd",
          is_admin: false,
          current_place_id: 3,
          has_accomodation: false,
          is_approved: false,
          chosen_address_id: 0,
          chosen_location_id: 0,
          chosen_place_id: 0,
          full_name: "Николаев Николай Николаевич",
          sex: "мужской",
          date_of_birth: "11.11.1993",
          age: 28,
          citizenship: "Российская Федерация",
          documents: "Паспорт, 1234 567890, 12.12.2013, МВД РФ, 770-221",
          registration: "Россия, Москва, ул. Лечебная, д. 16, кв. 398",
          phone: "+79859859855",
          is_contract: true,
          createdAt: new Date(),
          updatedAt: new Date(),
        },

        {
          email: "test2@hse.ars",
          password: "aadd",
          is_admin: false,
          current_place_id: 8,
          has_accomodation: true,
          is_approved: false,
          chosen_address_id: 0,
          chosen_location_id: 0,
          chosen_place_id: 8,
          full_name: "Николаев Николай Николаевич",
          sex: "мужской",
          date_of_birth: "11.11.1993",
          age: 28,
          citizenship: "Российская Федерация",
          documents: "Паспорт, 1234 567890, 12.12.2013, МВД РФ, 770-221",
          registration: "Россия, Москва, ул. Лечебная, д. 16, кв. 398",
          phone: "+79859859855",
          is_contract: true,
          createdAt: new Date(),
          updatedAt: new Date(),
        },

        {
          email: "admin@hse.ars",
          password: "aadd",
          is_admin: true,
          current_place_id: 0,
          has_accomodation: false,
          is_approved: false,
          chosen_address_id: 0,
          chosen_location_id: 0,
          chosen_place_id: 0,
          full_name: "Николаев Николай Николаевич",
          sex: "мужской",
          date_of_birth: "11.11.1993",
          age: 28,
          citizenship: "Российская Федерация",
          documents: "Паспорт, 1234 567890, 12.12.2013, МВД РФ, 770-221",
          registration: "Россия, Москва, ул. Лечебная, д. 16, кв. 398",
          phone: "+79859859855",
          is_contract: true,
          createdAt: new Date(),
          updatedAt: new Date(),
        },
      ],
      {}
    );
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete("Users", null, {});
  },
};
