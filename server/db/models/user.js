"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class User extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {}
  }
  User.init(
    {
      email: {
        type: DataTypes.STRING,
        unique: true,
        allowNull: false,
        validate: {
          isEmail: true, // TODO: Write test
        },
      },
      password: DataTypes.STRING,
      is_admin: DataTypes.BOOLEAN,
      current_place_id: DataTypes.INTEGER,
      has_accomodation: DataTypes.BOOLEAN,
      is_approved: DataTypes.BOOLEAN,
      chosen_address_id: DataTypes.INTEGER,
      chosen_location_id: DataTypes.INTEGER,
      chosen_place_id: DataTypes.INTEGER,
      full_name: DataTypes.STRING,
      sex: DataTypes.STRING,
      date_of_birth: DataTypes.STRING,
      age: DataTypes.INTEGER,
      citizenship: DataTypes.STRING,
      documents: DataTypes.STRING,
      registration: DataTypes.STRING,
      phone: DataTypes.STRING,
      is_contract: DataTypes.BOOLEAN,
    },
    {
      sequelize,
      modelName: "User",
    }
  );
  return User;
};
