'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Location extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  Location.init({
    address_id: DataTypes.INTEGER,
    pic: DataTypes.STRING,
    name: DataTypes.STRING,
    address: DataTypes.STRING,
    unoccupied: DataTypes.INTEGER,
    price_min: DataTypes.INTEGER,
    price_max: DataTypes.INTEGER,
    description: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'Location',
  });
  return Location;
};
