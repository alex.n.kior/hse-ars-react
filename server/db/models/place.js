'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Place extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  Place.init({
    pic: DataTypes.STRING,
    address_id: DataTypes.INTEGER,
    location_id: DataTypes.INTEGER,
    place_type: DataTypes.STRING,
    sex: DataTypes.STRING,
    cost_per_month: DataTypes.INTEGER,
    deposit: DataTypes.INTEGER,
    address: DataTypes.STRING,
    rooms: DataTypes.INTEGER,
    occupied: DataTypes.INTEGER,
    unoccupied_in_room: DataTypes.INTEGER,
    description: DataTypes.STRING,
    bed_type: DataTypes.STRING,
    bed_place: DataTypes.STRING,
    place_plan: DataTypes.STRING,
    floor_plan: DataTypes.STRING,
    starts: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'Place',
  });
  return Place;
};
