const express = require("express");
const logger = require("morgan");
const cors = require("cors");
const bodyParser = require("body-parser");
const app = express();
const generatePdf = require("./generatePdf");
const sequelize = require("./db/db");
const { User } = require("./db/models");
const { Address } = require("./db/models");
const { Location } = require("./db/models");
const { Place } = require("./db/models");
require("dotenv").config();

const session = require("express-session");

const PORT = process.env.PORT ?? 3001;

app.use(express.json());
app.use(cors());

app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Credentials", "true");
  res.header(
    "Access-Control-Allow-Headers",
    "Origin,Content-Type, Authorization, x-id, Content-Length, X-Requested-With"
  );
  res.header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS");
  next();
});

app.use(
  session({
    name: "sId",
    saveUninitialized: false,
    secret: "slksvj",
    resave: false,
  })
);

app.use(logger("dev"));
app.use(bodyParser.json());

app.route("/api/user/signin").post(async (req, res) => {
  const { email, password } = req.body;
  if (email && password) {
    const currentUser = await User.findOne({ where: { email } });
    if (currentUser && password == currentUser.password) {
      req.session.user = {
        email: currentUser.email,
        password: currentUser.password,
      };
      return res.json(currentUser);
    } else {
      return res.sendStatus(401);
    }
  }
});

app.route("/api/logout").get(async (req, res) => {
  req.session.destroy();
  res.clearCookie("sId");
});

app.route("/api/addresses").get(async (req, res) => {
  const addresses = await Address.findAll({ raw: true });

  res.json(addresses);
});

app.route("/api/locations/:id").get(async (req, res) => {
  const { id } = req.params;
  const locations = await Location.findAll({ where: { address_id: id } });

  res.json(locations);
});

app.route("/api/places/:id").get(async (req, res) => {
  const { id } = req.params;
  const places = await Place.findAll({ where: { location_id: id } });

  res.json(places);
});

app.route("/api/address/:id").get(async (req, res) => {
  const chosenid = req.params.id;
  const chosenAddress = await Address.findAll({ where: { id: chosenid } });
  res.json(chosenAddress);
});

app.route("/api/location/:id").get(async (req, res) => {
  const chosenid = req.params.id;
  const chosenLocation = await Location.findAll({ where: { id: chosenid } });
  res.json(chosenLocation);
});

app.route("/api/place/:id").get(async (req, res) => {
  const chosenid = req.params.id;
  const chosenPlace = await Place.findAll({ where: { id: chosenid } });
  res.json(chosenPlace);
});

app.route("/api/place/current").post(async (req, res) => {
  const { placeId } = req.body;

  const currentPlace = await Place.findAll({ where: { id: placeId } });
  res.json(currentPlace);
});

app.route("/api/currentuser").post(async (req, res) => {
  const { data } = req.body;
  const email = data;
  const currentuser = await User.findOne({
    where: { email },
  });
  res.json(currentuser);
});

app.route("/api/updateuser/address/:id").patch(async (req, res) => {
  const { id } = req.params;
  const { chosen_address } = req.body;

  const newUserAddressId = await User.update(
    { chosen_address_id: chosen_address },
    { where: { id } }
  );
  res.sendStatus(200);
});

app.route("/api/updateuser/location/:id").patch(async (req, res) => {
  const { id } = req.params;
  const { chosen_location } = req.body;

  const newUserLocationId = await User.update(
    { chosen_location_id: chosen_location },
    { where: { id } }
  );
  res.sendStatus(200);
});

app.route("/api/updateuser/isapproved").patch(async (req, res) => {
  const id = req.body.userId;

  const newUserIsApprovedId = await User.update(
    { is_approved: true },
    { where: { id } }
  );
  res.sendStatus(200);
});

app.route("/api/updateuser/request/drop/:id").patch(async (req, res) => {
  const { id } = req.params;

  const newUserAddressId = await User.update(
    {
      is_approved: false,
      chosen_address_id: 0,
      chosen_location_id: 0,
      chosen_place_id: 0,
    },
    { where: { id } }
  );
  res.sendStatus(200);
});

app.route("/api/updateuser/place/:id").patch(async (req, res) => {
  const { id } = req.params;
  const { chosen_place } = req.body;

  const newUserPlaceId = await User.update(
    { chosen_place_id: chosen_place },
    { where: { id } }
  );
  res.sendStatus(200);
});

app.route("/api/user/pdf").post(async (req, res) => {
  const { data } = req.body;
  const pdf = await generatePdf(`
  <html>
  <head>
  <title>Test PDF for ${data}</title>
  </head>
  <body>
  // The contents of our PDF will go here...
  </body>
  </html>
  `);

  const userPdf = pdf

  
  console.log(
    ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>",
    userPdf
  );
  res.json(userPdf);
});

app.use(express.urlencoded({ extended: true }));

(async () => {
  try {
    await sequelize.sync({ force: false });
    console.log("app ready");
    app.listen(PORT, () =>
      console.log(`Server listening on http://localhost:${PORT}`)
    );
  } catch (error) {
    console.error(error);
  }
})();
