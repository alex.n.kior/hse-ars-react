export function reportWebVitals(onPerfEntry) {
    if (onPerfEntry && onPerfEntry instanceof Function) {
        import('web-vitals').then(({ getCls, getFid, getFcp, getLcp, getTtfb }) => {
            getCls(onPerfEntry);
            getFid(onPerfEntry);
            getFcp(onPerfEntry);
            getLcp(onPerfEntry);
            getTtfb(onPerfEntry);
        });
    }
}
