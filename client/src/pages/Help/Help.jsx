import React, { useState, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { BrowserRouter as Route, Switch, Link, Router } from 'react-router-dom';

import { FormControl, InputLabel } from '@mui/material';
import { Button } from '@hse-design/react';
import { Input } from '@hse-design/react';
import { Card } from '../../../node_modules/@hse-design/react/lib/es/components/Card';
import ArsStepper from '../../components/ArsStepper/ArsStepper.jsx';
import AddressCards from '../../components/AddressCards/AddressCards.jsx';
import LocationCards from '../../components/LocationCards/LocationCards.jsx';
import PlaceCards from '../../components/PlaceCards/PlaceCards.jsx';
import { ArsFilter } from '../../components/ArsFilter/ArsFilter.jsx';
import { TextArea } from '@hse-design/react';
import { getCurrentUser } from '../../redux/actions/currentuser.actions';

export function Help() {
    const { current_user, isLoading, error } = useSelector((state) => state.currentUser);

    const defaultValues = {
        password: '',
        email: '',
    };
    const [value, setDefaultValues] = useState(defaultValues);
    const { email, password } = value;
    const handleChange = (event) => {
        const { target } = event;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const { name } = target;
        setDefaultValues((prevState) => ({
            ...prevState,
            [name]: value,
        }));
    };
    const submitForm = (event) => {
        event.preventDefault();
    };
  
    const [textValue, setTextValue] = useState('')
  const onChange = e => setTextValue(e.target.textValue)
    return (
        <>
            <section className="h-auto w-full flex flex-col justify-start content-center pt-8">
                <h2 className="self-center">Консультация с дирекцией</h2>
            </section>

            <Card className="w-128 h-auto border-2 mt-8 flex flex-col justify-between self-center">
                <form onSubmit={(event) => submitForm(event)}>
                    <FormControl margin="normal" required fullWidth>
              <TextArea
                className="w-full"
                    onChange={onChange}
                    value={textValue}
                placeholder="Текст сообщения"
              ></TextArea>
                    </FormControl>
                    <Button
                        type="submit"
                        variant="primary"
                        size="medium"
                        className="mt-4 self-center"
                    >
                        Отправить
                    </Button>
                </form>
            </Card>
        </>
    );
}
