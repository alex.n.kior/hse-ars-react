import React, { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { BrowserRouter as Route, Switch, Link, Router } from "react-router-dom";

import ArsStepper from "../../components/ArsStepper/ArsStepper.jsx";
import AddressCards from "../../components/AddressCards/AddressCards.jsx";
import LocationCards from "../../components/LocationCards/LocationCards.jsx";
import PlaceCards from "../../components/PlaceCards/PlaceCards.jsx";
import { ArsFilter } from "../../components/ArsFilter/ArsFilter.jsx";
import { getCurrentUser } from "../../redux/actions/currentuser.actions";

import { ResidenceUserInfo } from "../../components/ResidenceUserInfo/ResidenceUserInfo.jsx";
import { ResidenceParams } from "../../components/ResidenceParams/ResidenceParams.jsx";
import { ResidenceStart } from "../../components/ResidenceStart/ResidenceStart.jsx";

export function Residence() {
  const { current_user, isLoading, error } = useSelector(
    (state) => state.currentUser
  );
  return (
    <>
      <section className="h-auto w-full flex flex-col justify-start content-center pt-8">
        <h2 className="self-center">
          {current_user?.is_approved || !current_user?.has_accomodation
            ? "Личный кабинет проживающего"
            : "Направление на заселение"}
        </h2>
      </section>

        {current_user?.is_approved || !current_user?.has_accomodation
          ? (
        <section className="h-auto w-full flex flex-col justify-start content-center pt-8 pb-4">
      <ResidenceParams />
      </section>
          )
          : (<ResidenceStart />)}

      {current_user?.has_accomodation ? null : (
        <section className="h-auto w-full flex flex-col justify-start content-center pt-8 pb-16">
          <ResidenceUserInfo />
        </section>
      )}
    </>
  );
}
