import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { BrowserRouter as Route, Switch, Link, Router } from 'react-router-dom';

import { ArsSignIn } from '../../components/ArsSignIn/ArsSignIn.jsx';
import { Button } from '@hse-design/react';
import { Card } from '../../../node_modules/@hse-design/react/lib/es/components/Card';

export function Auth() {
    return (
        <>
        <Card className="s:w-[60%] m:w-[40%] l:w-[20%] h-auto border-2 mt-16 flex flex-col justify-between self-center">
        <ArsSignIn />
            </Card>
        </>
    );
}
