import React, { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { BrowserRouter as Route, Switch, Link, Router } from "react-router-dom";
import { Request } from "../../components/Request/Request.jsx";
import ArsStepper from "../../components/ArsStepper/ArsStepper.jsx";
import AddressCards from "../../components/AddressCards/AddressCards.jsx";
import LocationCards from "../../components/LocationCards/LocationCards.jsx";
import PlaceCards from "../../components/PlaceCards/PlaceCards.jsx";
import { ArsFilter } from "../../components/ArsFilter/ArsFilter.jsx";
import { getCurrentUser } from '../../redux/actions/currentuser.actions';

export function Booking() {
  const { current_user, isLoading, error } = useSelector(
    (state) => state.currentUser
  );

  function stepper() {
    if (current_user.chosen_address_id && current_user.chosen_location_id) {
      return 2;
    } else if (current_user.chosen_address_id) {
      return 1;
    }
    return 0;
  }

  return (
    <>
      <section className="h-auto w-full flex flex-col justify-start content-center pt-8">
        <h2 className="self-center">Бронирование нового варианта размещения</h2>
      </section>
      {current_user.chosen_place_id ? null : <ArsStepper step={stepper()} />}

      {current_user.chosen_address_id &&
      current_user.chosen_location_id &&
      current_user.chosen_place_id ? (
          <>
            <Request/>
          </>
      ) : current_user.chosen_address_id && current_user.chosen_location_id ? (
        <>
          <ArsFilter />
          <PlaceCards />
        </>
      ) : current_user.chosen_address_id ? (
        <LocationCards />
      ) : (
        <AddressCards />
      )}
    </>
  );
}
