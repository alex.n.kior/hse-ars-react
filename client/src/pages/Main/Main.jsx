import React, { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
  BrowserRouter as Route,
  Switch,
  Link,
  Router,
  useHistory,
} from "react-router-dom";
import { useRoutes, useRedirect } from "hookrouter";

import { Auth } from "../../pages/Auth/Auth.jsx";
import { Booking } from "../../pages/Booking/Booking.jsx";
import { Residence } from "../Residence/Residence.jsx";
import { Help } from "../Help/Help.jsx";

function Main() {
  const { current_user, isLoading, error } = useSelector(
    (state) => state.currentUser
  );
  const history = useHistory();

  const routes = {
    "/": () => <Residence />,
    "/Booking": () => <Booking />,
    "/Help": () => <Help />,
    "/Auth": () => <Auth />,
  };

  {
    localStorage.getItem("isAuth")
      ? null
      : useRedirect(history.location.pathname, "/Auth");
  }
  {
    current_user?.is_approved || !current_user?.has_accomodation
      ? null
      : useRedirect("/Booking", "/");
  }
  const routeResults = useRoutes(routes);

  return <>{routeResults || <h1>PAGE NOT FOUND</h1>}</>;
}

export default Main;
