import React, { useState, useEffect } from "react";
import { BrowserRouter as Route, Switch, Link } from 'react-router-dom';
import { memo } from 'react';
import { useDispatch, useSelector } from "react-redux";
import ArsHeader from './components/ArsHeader/ArsHeader.jsx'
import Main from './pages/Main/Main.jsx'
import ArsFooter from "./components/ArsFooter/ArsFooter.jsx";
import { getCurrentUser } from "./redux/actions/currentuser.actions";

function App() {
  const dispatch = useDispatch();
  const user = localStorage.getItem("isAuth")

  useEffect(() => {
    dispatch(getCurrentUser({user}));
}, []);
  return (
    <>
      <div className="h-full w-full flex flex-col justify-between">
        <ArsHeader />
        <main className="bg-[#F8F9FB] min-h-[70vh] w-auto flex flex-col justify-start pl-[4%] l:pl-[8%] xl:pl-[16%] xxl:pl-[32%] pr-[4%] l:pr-[8%] xl:pr-[16%] xxl:pr-[32%]">
            <Main />
        </main>
        <ArsFooter />
      </div>
    </>
  );
}

export default memo(App);
