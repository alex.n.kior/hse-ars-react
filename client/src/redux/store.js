import { applyMiddleware, createStore } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import createSagaMiddleware from 'redux-saga';

import { reducer } from './reducers/root.reducer';
import { rootSaga } from './sagas/root.saga';
import { initialReduxState } from './state';

export const sagaMiddleware = createSagaMiddleware();

export const store = createStore(
    reducer,
    initialReduxState,
    composeWithDevTools(applyMiddleware(sagaMiddleware)),
);
sagaMiddleware.run(rootSaga);
