const initialReduxState = {
  userSignIn: {
    userSignIn: {},
    isLoading: false,
    error: null,
    // isAuth: localStorage.getItem('isAuth'),
  },

  userSignUp: {
    userSignUp: {},
    isLoading: false,
    error: null,
    // isAuth: localStorage.getItem('isAuth'),
    // isRegister: localStorage.getItem('token'),
  },

  currentUser: {
    current_user: {},
    isLoading: false,
    error: null,
  },

  currentPlace: {
    current_place: [],
    isLoading: false,
    error: null,
  },

  addressCards: {
    addressCardsList: [],
    isLoading: false,
    error: null,
  },

  locationCards: {
    locationCardsList: [],
    isLoading: false,
    error: null,
  },

  placeCards: {
    placeCardsList: [],
    isLoading: false,
    error: null,
  },

  chosenAddress: {
    chosen_address: [],
    isLoading: false,
    error: null,
  },

  chosenLocation: {
    chosen_location: [],
    isLoading: false,
    error: null,
  },

  chosenPlace: {
    chosen_place: [],
    isLoading: false,
    error: null,
  },

  userAddress: {
    chosenAddress: [],
    isLoading: false,
    error: null,
  },

  userLocation: {
    chosenLocation: [],
    isLoading: false,
    error: null,
  },

  userPlace: {
    chosenPlace: [],
    isLoading: false,
    error: null,
  },

  userPdf: {
    user_pdf: {},
    isLoading: false,
    error: null,
  },
};

export { initialReduxState };
