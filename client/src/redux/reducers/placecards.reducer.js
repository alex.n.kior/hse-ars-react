import { SET_PLACE_CARDS, SET_ERROR, SET_LOADING } from '../types';

export function placeCardsReducer(places = {}, action) {
    const { type, payload, error } = action;

    switch (type) {
        case SET_ERROR: {
            return { placeCardsList: places.placeCardsList, isLoading: false, error };
        }

        case SET_LOADING: {
            return { placeCardsList: places.placeCardsList, isLoading: true, error: null };
        }

        case SET_PLACE_CARDS: {
            const { places } = payload;
            return { placeCardsList: places, isLoading: false, error: null };
        }

        default:
            return places;
    }
}
