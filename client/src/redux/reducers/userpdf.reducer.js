import { SET_ERROR, SET_LOADING, USER_SET_PDF } from '../types';

export function userPdfReducer(userPdf = {}, action) {
    const { type, payload, error } = action;

    switch (type) {
        case SET_ERROR: {
            return { user_pdf: userPdf.user_pdf, isLoading: false, error };
        }

        case SET_LOADING: {
            return { user_pdf: userPdf.user_pdf, isLoading: true, error: null };
        }

        case USER_SET_PDF: {
            const { userPdf } = payload;
            return { user_pdf: userPdf, isLoading: false, error: null };
        }

        default:
            return userPdf;
    }
}
