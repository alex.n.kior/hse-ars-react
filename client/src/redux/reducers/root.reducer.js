import { combineReducers } from "redux";

import { userSignUpReducer } from "./usersignup.reducer";
import { userSignInReducer } from "./usersignin.reducer";
import { currentUserReducer } from "./currentuser.reducer";
import { currentPlaceReducer } from "./currentplace.reducer";
import { userAddressReducer } from "./useraddress.reducer";
import { userLocationReducer } from "./userlocation.reducer";
import { userPlaceReducer } from "./userplace.reducer";
import { addressCardsReducer } from "./addresscards.reducer";
import { locationCardsReducer } from "./locationcards.reducer";
import { placeCardsReducer } from "./placecards.reducer";
import { chosenAddressReducer } from "./chosenaddress.reducer";
import { chosenLocationReducer } from "./chosenlocation.reducer";
import { chosenPlaceReducer } from "./chosenplace.reducer";
import { userApprovedReducer } from './userapproved.reducer'
import { dropRequestReducer } from "./droprequest.reducer";
import { userPdfReducer } from './userpdf.reducer'

export const reducer = combineReducers({
  userSignUp: userSignUpReducer,
  userSignIn: userSignInReducer,
  addressCards: addressCardsReducer,
  locationCards: locationCardsReducer,
  placeCards: placeCardsReducer,
  userAddress: userAddressReducer,
  userLocation: userLocationReducer,
  userPlace: userPlaceReducer,
  currentUser: currentUserReducer,
  currentPlace: currentPlaceReducer,
  chosenAddress: chosenAddressReducer,
  chosenLocation: chosenLocationReducer,
  chosenPlace: chosenPlaceReducer,
  approvedUser: userApprovedReducer,
  dropRequest: dropRequestReducer,
  userPdf: userPdfReducer
});
