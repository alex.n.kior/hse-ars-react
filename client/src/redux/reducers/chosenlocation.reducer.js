import { SET_ERROR, SET_LOADING, SET_LOCATION } from '../types';

export function chosenLocationReducer(chosenLocation = {}, action) {
    const { type, payload, error } = action;

    switch (type) {
        case SET_ERROR: {
            return { chosen_location: chosenLocation.chosen_location, isLoading: false, error };
        }

        case SET_LOADING: {
            return { chosen_location: chosenLocation.chosen_location, isLoading: true, error: null };
        }

        case SET_LOCATION: {
            const { chosenLocation } = payload;
            return { chosen_location: chosenLocation, isLoading: false, error: null };
        }

        default:
            return chosenLocation;
    }
}
