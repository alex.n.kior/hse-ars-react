import { SET_ERROR, SET_LOADING, USER_APPROVED } from '../types';

export function userApprovedReducer(userApproved = {}, action) {
    const { type, payload, error } = action;

    switch (type) {
        case SET_ERROR: {
            return { approvedUser: userApproved.approvedUser, isLoading: false, error };
        }

        case SET_LOADING: {
            return { approvedUser: userApproved.approvedUser, isLoading: true, error: null };
        }

        case USER_APPROVED: {
            const { userApproved } = payload;
            return {
                ...userApproved,
                approvedUser: userApproved,
                isLoading: false,
                error: null,
            };
        }

        default: {
            return userApproved;
        }
    }
}
