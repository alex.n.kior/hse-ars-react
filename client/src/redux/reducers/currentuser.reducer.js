import { SET_ERROR, SET_LOADING, SET_USER } from '../types';

export function currentUserReducer(user = {}, action) {
    const { type, payload, error } = action;

    switch (type) {
        case SET_ERROR: {
            return { current_user: user.current_user, isLoading: false, error };
        }

        case SET_LOADING: {
            return { current_user: user.current_user, isLoading: true, error: null };
        }

        case SET_USER: {
            const { user } = payload;
            return { current_user: user, isLoading: false, error: null };
        }

        default:
            return user;
    }
}
