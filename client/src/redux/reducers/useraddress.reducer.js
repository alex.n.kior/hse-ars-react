import { SET_ERROR, SET_LOADING, USER_ADD_ADDRESS } from '../types';

export function userAddressReducer(userAddress = {}, action) {
    const { type, payload, error } = action;

    switch (type) {
        case SET_ERROR: {
            return { chosenAddress: userAddress.chosenAddress, isLoading: false, error };
        }

        case SET_LOADING: {
            return { chosenAddress: userAddress.chosenAddress, isLoading: true, error: null };
        }

        case USER_ADD_ADDRESS: {
            const { editedUserAddress } = payload;
            return {
                ...userAddress,
                chosenAddress: editedUserAddress,
                isLoading: false,
                error: null,
            };
        }

        default: {
            return userAddress;
        }
    }
}
