import { SET_ERROR, SET_LOADING, USER_ADD_LOCATION } from '../types';

export function userLocationReducer(userLocation = {}, action) {
    const { type, payload, error } = action;

    switch (type) {
        case SET_ERROR: {
            return { chosenLocation: userLocation.chosenLocation, isLoading: false, error };
        }

        case SET_LOADING: {
            return { chosenLocation: userLocation.chosenLocation, isLoading: true, error: null };
        }

        case USER_ADD_LOCATION: {
            const { editedUserLocation } = payload;
            return {
                ...userLocation,
                chosenLocation: editedUserLocation,
                isLoading: false,
                error: null,
            };
        }

        default: {
            return userLocation;
        }
    }
}
