import { SET_ADDRESS_CARDS, SET_ERROR, SET_LOADING } from '../types';

export function addressCardsReducer(addresses = {}, action) {
    const { type, payload, error } = action;

    switch (type) {
        case SET_ERROR: {
            return { addressCardsList: addresses.addressCardsList, isLoading: false, error };
        }

        case SET_LOADING: {
            return { addressCardsList: addresses.addressCardsList, isLoading: true, error: null };
        }

        case SET_ADDRESS_CARDS: {
            const { addresses } = payload;
            return { addressCardsList: addresses, isLoading: false, error: null };
        }

        default:
            return addresses;
    }
}
