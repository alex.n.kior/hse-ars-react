import { SET_LOCATION_CARDS, SET_ERROR, SET_LOADING } from '../types';

export function locationCardsReducer(locations = {}, action) {
    const { type, payload, error } = action;

    switch (type) {
        case SET_ERROR: {
            return { locationCardsList: locations.locationCardsList, isLoading: false, error };
        }

        case SET_LOADING: {
            return { locationCardsList: locations.locationCardsList, isLoading: true, error: null };
        }

        case SET_LOCATION_CARDS: {
            const { locations } = payload;
            return { locationCardsList: locations, isLoading: false, error: null };
        }

        default:
            return locations;
    }
}
