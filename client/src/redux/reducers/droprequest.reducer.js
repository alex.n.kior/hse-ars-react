import { SET_ERROR, SET_LOADING, USER_DROP_REQUEST } from '../types';

export function dropRequestReducer(request = {}, action) {
    const { type, payload, error } = action;

    switch (type) {
        case SET_ERROR: {
            return { dropRequest: request.dropRequest, isLoading: false, error };
        }

        case SET_LOADING: {
            return { dropRequest: request.dropRequest, isLoading: true, error: null };
        }

        case USER_DROP_REQUEST: {
            const { request } = payload;
            return {
                ...request,
                dropRequest: request,
                isLoading: false,
                error: null,
            };
        }

        default: {
            return request;
        }
    }
}
