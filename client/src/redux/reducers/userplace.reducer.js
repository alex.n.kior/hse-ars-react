import { SET_ERROR, SET_LOADING, USER_ADD_PLACE } from '../types';

export function userPlaceReducer(userPlace = {}, action) {
    const { type, payload, error } = action;

    switch (type) {
        case SET_ERROR: {
            return { chosenPlace: userPlace.chosenPlace, isLoading: false, error };
        }

        case SET_LOADING: {
            return { chosenPlace: userPlace.chosenPlace, isLoading: true, error: null };
        }

        case USER_ADD_PLACE: {
            const { editedUserPlace } = payload;
            return {
                ...userPlace,
                chosenPlace: editedUserPlace,
                isLoading: false,
                error: null,
            };
        }

        default: {
            return userPlace;
        }
    }
}
