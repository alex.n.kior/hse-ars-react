import { SET_ERROR, SET_LOADING, SET_PLACE } from '../types';

export function chosenPlaceReducer(chosenPlace = {}, action) {
    const { type, payload, error } = action;

    switch (type) {
        case SET_ERROR: {
            return { chosen_place: chosenPlace.chosen_place, isLoading: false, error };
        }

        case SET_LOADING: {
            return { chosen_place: chosenPlace.chosen_place, isLoading: true, error: null };
        }

        case SET_PLACE: {
            const { chosenPlace } = payload;
            return { chosen_place: chosenPlace, isLoading: false, error: null };
        }

        default:
            return chosenPlace;
    }
}
