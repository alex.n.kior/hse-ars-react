import { initialReduxState } from '../state';
import { USER_CREATE, USER_ENTER, USER_FAILED, USER_SUCCESS } from '../types';

function authReducer(state = initialReduxState.auth, action = {}) {
    const { type = '', payload = {} } = action;
    switch (type) {
        case USER_CREATE:
            return {
                loading: true,
                ...payload,
            };
        case USER_ENTER:
            return {
                loading: true,
                ...payload,
            };
        case USER_SUCCESS:
            return {
                loading: true,
                ...payload,
            };
        case USER_FAILED:
            return {
                loading: true,
                ...payload,
            };
        default:
            return state;
    }
}

export { authReducer };
