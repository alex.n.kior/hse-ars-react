import { SET_ERROR, SET_LOADING, SET_CURRENT_PLACE } from '../types';

export function currentPlaceReducer(currentPlace = {}, action) {
    const { type, payload, error } = action;

    switch (type) {
        case SET_ERROR: {
            return { current_place: currentPlace.current_place, isLoading: false, error };
        }

        case SET_LOADING: {
            return { current_place: currentPlace.current_place, isLoading: true, error: null };
        }

        case SET_CURRENT_PLACE: {
            const { currentPlace } = payload;
            return { current_place: currentPlace, isLoading: false, error: null };
        }

        default:
            return currentPlace;
    }
}
