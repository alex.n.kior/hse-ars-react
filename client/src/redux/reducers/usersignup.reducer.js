import { SET_ERROR, SET_LOADING, USER_SIGN_UP } from '../types';

export function userSignUpReducer(signUpForm = {}, action) {
    const { type, payload, error } = action;

    switch (type) {
        case SET_ERROR: {
            return { userSignUp: signUpForm.userSignUp, isLoading: false, error };
        }

        case SET_LOADING: {
            return { userSignUp: signUpForm.userSignUp, isLoading: true, error: null };
        }

        case USER_SIGN_UP: {
            const { signUpForm } = payload;
            return {
                userSignUp: signUpForm,
                isLoading: false,
                error: null,
            };
        }

        default: {
            return signUpForm;
        }
    }
}
