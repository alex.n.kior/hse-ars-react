import { SET_ERROR, SET_LOADING, SET_ADDRESS } from '../types';

export function chosenAddressReducer(chosenAddress = {}, action) {
    const { type, payload, error } = action;

    switch (type) {
        case SET_ERROR: {
            return { chosen_address: chosenAddress.chosen_address, isLoading: false, error };
        }

        case SET_LOADING: {
            return { chosen_address: chosenAddress.chosen_address, isLoading: true, error: null };
        }

        case SET_ADDRESS: {
            const { chosenAddress } = payload;
            return { chosen_address: chosenAddress, isLoading: false, error: null };
        }

        default:
            return chosenAddress;
    }
}
