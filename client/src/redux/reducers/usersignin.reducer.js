import { SET_ERROR, SET_LOADING, USER_SIGN_IN, USER_GET_SIGN_IN } from '../types';

export function userSignInReducer(signInForm = {}, action) {
    const { type, payload, error } = action;

    switch (type) {
        case SET_ERROR: {
            return { userSignIn: signInForm.userSignIn, isLoading: false, error };
        }

        case SET_LOADING: {
            return { userSignIn: signInForm.userSignIn, isLoading: true, error: null };
        }

        case USER_SIGN_IN: {
            const { signInForm } = payload;
            return {
                userSignIn: signInForm,
                isLoading: false,
                error: null,
            };
        }

        default: {
            return signInForm;
        }
    }
}
