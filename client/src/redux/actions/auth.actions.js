import { USER_CREATE, USER_ENTER, USER_FAILED, USER_SUCCESS } from '../types';

export function userCreate(payload) {
    return {
        type: USER_CREATE,
        loading: false,
        ...payload,
    };
}

export function userEnter(payload) {
    return {
        type: USER_ENTER,
        loading: false,
        ...payload,
    };
}

export function userCreateSuccess(payload) {
    return {
        type: USER_SUCCESS,
        loading: false,
        payload,
    };
}

export function userCreateFailed(payload) {
    return {
        type: USER_FAILED,
        loading: false,
        payload: {
            ...payload,
        },
    };
}
