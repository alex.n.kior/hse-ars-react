import { SET_ERROR, SET_LOADING, USER_APPROVED } from '../types';

export function setUserApproved({ userId }) {
    return {
        type: USER_APPROVED,
        payload: { userId },
    };
}

export function setLoading() {
    return {
        type: SET_LOADING,
    };
}

export function setError(error) {
    return {
        type: SET_ERROR,
        error,
    };
}
