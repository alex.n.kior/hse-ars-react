import { SET_ERROR, SET_LOADING, USER_DROP_REQUEST } from '../types';

export function setDropRequest({ userId }) {
    return {
        type: USER_DROP_REQUEST,
        payload: { userId },
    };
}

export function setLoading() {
    return {
        type: SET_LOADING,
    };
}

export function setError(error) {
    return {
        type: SET_ERROR,
        error,
    };
}
