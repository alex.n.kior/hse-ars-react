import { GET_CURRENT_PLACE, SET_ERROR, SET_LOADING, SET_CURRENT_PLACE } from '../types';

export function getCurrentPlace({ placeId }) {
    return {
        type: GET_CURRENT_PLACE,
        payload: { placeId },
    };
}

export function setCurrentPlace(currentPlace) {
    return {
        type: SET_CURRENT_PLACE,
        payload: { currentPlace },
    };
}

export function setLoading() {
    return {
        type: SET_LOADING,
    };
}

export function setError(error) {
    return {
        type: SET_ERROR,
        error,
    };
}
