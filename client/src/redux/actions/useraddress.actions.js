import { SET_ERROR, SET_LOADING, USER_ADD_ADDRESS } from '../types';

export function setUserAddress({ userAddressId, userId }) {
    return {
        type: USER_ADD_ADDRESS,
        payload: { userAddressId, userId },
    };
}

export function setLoading() {
    return {
        type: SET_LOADING,
    };
}

export function setError(error) {
    return {
        type: SET_ERROR,
        error,
    };
}
