import { SET_ERROR, SET_LOADING, USER_ADD_LOCATION } from '../types';

export function setUserLocation({ userLocationId, userId }) {
    return {
        type: USER_ADD_LOCATION,
        payload: { userLocationId, userId },
    };
}

export function setLoading() {
    return {
        type: SET_LOADING,
    };
}

export function setError(error) {
    return {
        type: SET_ERROR,
        error,
    };
}
