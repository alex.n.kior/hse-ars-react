import { SET_ERROR, SET_LOADING, USER_SIGN_IN } from '../types';

export function setUserSignIn({ signInForm }) {
    return {
        type: USER_SIGN_IN,
        payload: { signInForm },
    };
}

export function setLoading() {
    return {
        type: SET_LOADING,
    };
}

export function setError(error) {
    return {
        type: SET_ERROR,
        error,
    };
}
