import { USER_GET_PDF, SET_ERROR, SET_LOADING, USER_SET_PDF } from "../types";

export function getUserPdf({ userId }) {
  return {
    type: USER_GET_PDF,
    payload: { userId },
  };
}

export function setUserPdf(userPdf) {
  return {
    type: USER_SET_PDF,
    payload: { userPdf },
  };
}

export function setLoading() {
  return {
    type: SET_LOADING,
  };
}

export function setError(error) {
  return {
    type: SET_ERROR,
    error,
  };
}
