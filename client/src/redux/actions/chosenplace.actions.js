import { GET_PLACE, SET_ERROR, SET_LOADING, SET_PLACE } from '../types';

export function getChosenPlace({ placeId }) {
    return {
        type: GET_PLACE,
        payload: { placeId },
    };
}

export function setChosenPlace(chosenPlace) {
    return {
        type: SET_PLACE,
        payload: { chosenPlace },
    };
}

export function setLoading() {
    return {
        type: SET_LOADING,
    };
}

export function setError(error) {
    return {
        type: SET_ERROR,
        error,
    };
}
