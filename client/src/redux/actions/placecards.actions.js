import { GET_PLACE_CARDS, SET_PLACE_CARDS, SET_ERROR, SET_LOADING } from '../types';

export function getPlaceCards({ locationId }) {
    return {
        type: GET_PLACE_CARDS,
        payload: { locationId },
    };
}

export function setPlaceCards(places) {
    return {
        type: SET_PLACE_CARDS,
        payload: { places },
    };
}

export function setLoading() {
    return {
        type: SET_LOADING,
    };
}

export function setError(error) {
    return {
        type: SET_ERROR,
        error,
    };
}
