import { SET_ERROR, SET_LOADING, USER_ADD_PLACE } from '../types';

export function setUserPlace({ userPlaceId, userId }) {
    return {
        type: USER_ADD_PLACE,
        payload: { userPlaceId, userId },
    };
}

export function setLoading() {
    return {
        type: SET_LOADING,
    };
}

export function setError(error) {
    return {
        type: SET_ERROR,
        error,
    };
}
