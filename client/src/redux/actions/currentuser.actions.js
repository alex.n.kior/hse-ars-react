import { GET_USER, SET_ERROR, SET_LOADING, SET_USER } from "../types";

export function getCurrentUser({ user }) {
  return {
    type: GET_USER,
    payload: { user },
  };
}

export function setCurrentUser(user) {
  return {
    type: SET_USER,
    payload: { user },
  };
}

export function setLoading() {
  return {
    type: SET_LOADING,
  };
}

export function setError(error) {
  return {
    type: SET_ERROR,
    error,
  };
}
