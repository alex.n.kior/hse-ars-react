import { GET_ADDRESS, SET_ERROR, SET_LOADING, SET_ADDRESS } from '../types';

export function getChosenAddress({ addressId }) {
    return {
        type: GET_ADDRESS,
        payload: { addressId },
    };
}

export function setChosenAddress(chosenAddress) {
    return {
        type: SET_ADDRESS,
        payload: { chosenAddress },
    };
}

export function setLoading() {
    return {
        type: SET_LOADING,
    };
}

export function setError(error) {
    return {
        type: SET_ERROR,
        error,
    };
}
