import { GET_ADDRESS_CARDS, SET_ADDRESS_CARDS, SET_ERROR, SET_LOADING } from '../types';

export function getAddressCards() {
    return {
        type: GET_ADDRESS_CARDS,
    };
}

export function setAddressCards(addresses) {
    return {
        type: SET_ADDRESS_CARDS,
        payload: { addresses },
    };
}

export function setLoading() {
    return {
        type: SET_LOADING,
    };
}

export function setError(error) {
    return {
        type: SET_ERROR,
        error,
    };
}
