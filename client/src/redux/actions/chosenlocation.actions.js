import { GET_LOCATION, SET_ERROR, SET_LOADING, SET_LOCATION } from '../types';

export function getChosenLocation({ locationId }) {
    return {
        type: GET_LOCATION,
        payload: { locationId },
    };
}

export function setChosenLocation(chosenLocation) {
    return {
        type: SET_LOCATION,
        payload: { chosenLocation },
    };
}

export function setLoading() {
    return {
        type: SET_LOADING,
    };
}

export function setError(error) {
    return {
        type: SET_ERROR,
        error,
    };
}
