import { GET_LOCATION_CARDS, SET_LOCATION_CARDS, SET_ERROR, SET_LOADING } from '../types';

export function getLocationCards({ addressId }) {
    return {
        type: GET_LOCATION_CARDS,
        payload: { addressId },
    };
}

export function setLocationCards(locations) {
    return {
        type: SET_LOCATION_CARDS,
        payload: { locations },
    };
}

export function setLoading() {
    return {
        type: SET_LOADING,
    };
}

export function setError(error) {
    return {
        type: SET_ERROR,
        error,
    };
}
