import { SET_ERROR, SET_LOADING, USER_SIGN_UP } from '../types';

export function setUserSignUp({ signUpForm }) {
    return {
        type: USER_SIGN_UP,
        payload: { signUpForm },
    };
}

export function setLoading() {
    return {
        type: SET_LOADING,
    };
}

export function setError(error) {
    return {
        type: SET_ERROR,
        error,
    };
}
