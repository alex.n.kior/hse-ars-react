export const GET_USERS = 'GET_USERS';
export const SET_USERS = 'SET_USERS';

export const USER_CREATE = 'USER_CREATE';
export const USER_ENTER = 'USER_ENTER';
export const USER_SIGN_UP = 'USER_SIGN_UP';
export const USER_SIGN_IN = 'USER_SIGN_IN';
export const USER_GET_SIGN_IN = 'USER_GET_SIGN_IN';
export const USER_ADD_ADDRESS = 'USER_ADD_ADDRESS';
export const USER_ADD_LOCATION = 'USER_ADD_LOCATION';
export const USER_ADD_PLACE = 'USER_ADD_PLACE';
export const USER_APPROVED = 'USER_APPROVED';
export const USER_DROP_REQUEST = 'USER_DROP_REQUEST';
export const USER_GET_PDF = 'USER_GET_PDF';
export const USER_SET_PDF = 'USER_SET_PDF';
export const USER_SUCCESS = 'USER_SUCCESS';
export const USER_FAILED = 'USER_FAILED';

export const GET_USER = 'GET_USER';
export const SET_USER = 'SET_USER';

export const GET_ADDRESS_CARDS = 'GET_ADDRESS_CARDS';
export const SET_ADDRESS_CARDS = 'SET_ADDRESS_CARDS';

export const GET_ADDRESS = 'GET_ADDRESS';
export const SET_ADDRESS = 'SET_ADDRESS';

export const GET_LOCATION_CARDS = 'GET_LOCATION_CARDS';
export const SET_LOCATION_CARDS = 'SET_LOCATION_CARDS';

export const GET_LOCATION = 'GET_LOCATION';
export const SET_LOCATION = 'SET_LOCATION';

export const GET_PLACE_CARDS = 'GET_PLACE_CARDS';
export const SET_PLACE_CARDS = 'SET_PLACE_CARDS';

export const GET_CURRENT_PLACE = 'GET_CURRENT_PLACE';
export const SET_CURRENT_PLACE = 'GET_CURRENT_PLACE';

export const GET_PLACE = 'GET_PLACE';
export const SET_PLACE = 'SET_PLACE';

export const SET_LOADING = 'SET_LOADING';
export const SET_ERROR = 'SET_ERROR';

export const REQUEST_API_DATA = 'REQUEST_API_DATA';
export const RECEIVE_API_DATA = 'RECEIVE_API_DATA';
