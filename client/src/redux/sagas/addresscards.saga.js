import axios from 'axios';
import { call, put, takeEvery } from 'redux-saga/effects';

import { setAddressCards, setError, setLoading } from '../actions/addresscards.actions';
import { GET_ADDRESS_CARDS } from '../types';

async function getAddressCardsFromServer() {
    try {
        const responce = await axios.get(`http://localhost:3001/api/addresses`);
        return responce.data;
    } catch (error) {
        throw Error(error);
    }
}

function* getAddressCardsWorker() {
    try {
        yield put(setLoading());
        const responce = yield call(getAddressCardsFromServer);

        yield put(setAddressCards(responce));
    } catch (error) {
        yield put(setError(error));
    }
}

export function* watchAddressCards() {
    yield takeEvery(GET_ADDRESS_CARDS, getAddressCardsWorker);
}
