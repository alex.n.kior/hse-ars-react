import axios from 'axios';
import { call, put, takeEvery } from 'redux-saga/effects';

import { setError, setLoading, setUserPlace } from '../actions/userplace.actions';
import { USER_ADD_PLACE } from '../types';

async function setUserPlaceToServer({ userPlaceId, userId }) {
    try {
        const response = await axios.patch(`http://localhost:3001/api/updateuser/place/${userId}`, {
            chosen_place: userPlaceId,
        });

        return response.status;
    } catch (error) {
        throw Error(error);
    }
}

function* setUserPlaceWorker(action) {
    try {
        yield put(setLoading());
        const response = yield call(setUserPlaceToServer, action.payload);

        yield put(setUserPlace(response));
    } catch (error) {
        yield put(setError(error));
    }
}

export function* watchUserPlace() {
    yield takeEvery(USER_ADD_PLACE, setUserPlaceWorker);
}
