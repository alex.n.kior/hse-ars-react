import { all, call, put, takeLatest } from 'redux-saga/effects';

import { userCreateFailed, userCreateSuccess } from '../actions/auth.actions';
import { USER_CREATE, USER_ENTER } from '../types';

function insertNewUser(user) {
    const config = {
        method: 'POST',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(user),
    };

    function status(response) {
        if (response.status >= 200 && response.status < 300) {
            return Promise.resolve(response);
        }
        return Promise.reject(new Error(response.statusText));
    }

    function json(response) {
        return response.json();
    }

    return fetch('http://localhost:3001/api/signup', config)
        .then(status)
        .then(json)
        .catch((error) => {
            throw new Error(error.message);
        });
}

async function loginUser(user) {
    try {
        const config = {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(user),
        };
        const response = await fetch('http://localhost:3001/api/signin', config);
        if (response.status >= 200 && response.status < 300) {
            return await Promise.resolve(response.json());
        }
        const result = await response.json();

        return await Promise.reject(new Error(result.message));
    } catch (error) {
        return error;
    }
}

function* postNewUser(params) {
    try {
        yield call(insertNewUser, params);
        yield put(
            userCreateSuccess({
                isRegister: true,
            }),
        );
    } catch (error) {
        yield put(
            userCreateFailed({
                errorMessage: error.message,
            }),
        );
    }
}

function* postEnterUser(params) {
    try {
        const result = yield call(loginUser, params);
        yield put(
            userCreateSuccess({
                userId: result.userid,
                isAuth: result.auth,
                token: result.token,
            }),
        );
    } catch (error) {
        yield put(
            userCreateFailed({
                errorMessage: error.message,
            }),
        );
    }
}

function* actionWatcher() {
    yield takeLatest(USER_CREATE, postNewUser);
    yield takeLatest(USER_ENTER, postEnterUser);
}

function* watchAuth() {
    yield all([actionWatcher()]);
}

export { watchAuth };
