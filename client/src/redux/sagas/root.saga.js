import { all } from "redux-saga/effects";

import { watchUserSignUp } from "./usersignup.saga";
import { watchUserSignIn } from "./usersignin.saga";
import { watchCurrentUser } from "./currentuser.saga";
import { watchCurrentPlace } from "./currentplace.saga";
import { watchAddressCards } from "./addresscards.saga";
import { watchLocationCards } from "./locationcards.saga";
import { watchPlaceCards } from "./placecards.saga";
import { watchUserAddress } from "./useraddress.saga";
import { watchUserLocation } from "./userlocation.saga";
import { watchUserPlace } from "./userplace.saga";
import { watchChosenAddress } from "./chosenaddress.saga";
import { watchChosenLocation } from "./chosenlocation.saga";
import { watchChosenPlace } from "./chosenplace.saga";
import { watchDropRequest } from "./droprequest.saga";
import { watchUserApproved } from './userapproved.saga';
import { watchUserPdf } from './userpdf.saga';

function* rootSaga() {
  yield all([
    watchUserSignUp(),
    watchUserSignIn(),
    watchAddressCards(),
    watchLocationCards(),
    watchPlaceCards(),
    watchCurrentUser(),
    watchCurrentPlace(),
    watchUserAddress(),
    watchUserLocation(),
    watchUserPlace(),
    watchChosenAddress(),
    watchChosenLocation(),
    watchChosenPlace(),
    watchUserApproved(),
    watchDropRequest(),
    watchUserPdf()
  ]);
}

export { rootSaga };
