import axios from 'axios';
import { call, put, takeEvery } from 'redux-saga/effects';

import { setChosenPlace, setError, setLoading } from '../actions/chosenplace.actions';
import { GET_PLACE } from '../types';

async function getChosenPlaceFromServer({placeId}) {
    try {
        const response = await axios.get(`http://localhost:3001/api/place/${placeId}`);
        return response.data;
    } catch (error) {
        throw Error(error);
    }
}

function* getChosenPlaceWorker(action) {
    try {
        yield put(setLoading());
        const response = yield call(getChosenPlaceFromServer, action.payload);

        yield put(setChosenPlace(response));
    } catch (error) {
        yield put(setError(error));
    }
}

export function* watchChosenPlace() {
    yield takeEvery(GET_PLACE, getChosenPlaceWorker);
}
