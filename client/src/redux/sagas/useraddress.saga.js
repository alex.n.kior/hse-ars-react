import axios from 'axios';
import { call, put, takeEvery } from 'redux-saga/effects';

import { setError, setLoading, setUserAddress } from '../actions/useraddress.actions';
import { USER_ADD_ADDRESS } from '../types';

async function setUserAddressToServer({ userAddressId, userId }) {
    try {
        const response = await axios.patch(`http://localhost:3001/api/updateuser/address/${userId}`, {
            chosen_address: userAddressId,
        });

        return response.status;
    } catch (error) {
        throw Error(error);
    }
}

function* setUserAddressWorker(action) {
    try {
        yield put(setLoading());
        const response = yield call(setUserAddressToServer, action.payload);

        yield put(setUserAddress(response));
    } catch (error) {
        yield put(setError(error));
    }
}

export function* watchUserAddress() {
    yield takeEvery(USER_ADD_ADDRESS, setUserAddressWorker);
}
