import axios from 'axios';
import { call, put, takeEvery } from 'redux-saga/effects';

import { setPlaceCards, setError, setLoading } from '../actions/placecards.actions';
import { GET_PLACE_CARDS } from '../types';

async function getPlaceCardsFromServer({locationId}) {
    try {
        const responce = await axios.get(`http://localhost:3001/api/places/${locationId}`);
        return responce.data;
    } catch (error) {
        throw Error(error);
    }
}

function* getPlaceCardsWorker(action) {
    try {
        yield put(setLoading());
        const responce = yield call(getPlaceCardsFromServer, action.payload);

        yield put(setPlaceCards(responce));
    } catch (error) {
        yield put(setError(error));
    }
}

export function* watchPlaceCards() {
    yield takeEvery(GET_PLACE_CARDS, getPlaceCardsWorker);
}
