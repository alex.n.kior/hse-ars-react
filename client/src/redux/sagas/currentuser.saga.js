import axios from "axios";
import { call, put, takeEvery } from "redux-saga/effects";

import {
  setCurrentUser,
  setError,
  setLoading,
} from "../actions/currentuser.actions";
import { GET_USER } from "../types";

async function getCurrentUserFromServer({ user }) {
  try {
    const response = await axios.post(`http://localhost:3001/api/currentuser`, {
      data: user,
    });
    return response.data;
  } catch (error) {
    throw Error(error);
  }
}

function* getCurrentUserWorker(action) {
  try {
    yield put(setLoading());
    const response = yield call(getCurrentUserFromServer, action.payload);

    yield put(setCurrentUser(response));
  } catch (error) {
    yield put(setError(error));
  }
}

export function* watchCurrentUser() {
  yield takeEvery(GET_USER, getCurrentUserWorker);
}
