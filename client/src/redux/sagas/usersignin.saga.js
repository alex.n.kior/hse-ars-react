import axios from "axios";
import { call, put, takeEvery } from "redux-saga/effects";

import { setError, setLoading, setUserSignIn } from "../actions/usersignin.actions";
import { USER_SIGN_IN } from "../types";

async function setUserSignInToServer({ signInForm }) {
  try {
    const response = await axios.post('http://localhost:3001/api/user/signin', {
      data: signInForm
    });

    return response.data;
  } catch (error) {
    throw Error(error);
  }
}

function* setUserSignInWorker(action) {
  try {
    yield put(setLoading());
    const response = yield call(setUserSignInToServer, action.payload);

    yield put(setUserSignIn(response));
  } catch (error) {
    yield put(setError(error));
  }
}

export function* watchUserSignIn() {
  yield takeEvery(USER_SIGN_IN, setUserSignInWorker);
}
