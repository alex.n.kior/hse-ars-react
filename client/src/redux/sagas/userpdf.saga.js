import axios from "axios";
import { call, put, takeEvery } from "redux-saga/effects";

import {
  setUserPdf,
  setError,
  setLoading,
} from "../actions/userpdf.actions";
import { USER_GET_PDF } from "../types";

async function getUserPdfFromServer({ userId }) {
  try {
    const response = await axios.post(`http://localhost:3001/api/user/pdf`, {
      data: userId,
    });
    return response.data;
  } catch (error) {
    throw Error(error);
  }
}

function* getUserPdfWorker(action) {
  try {
    yield put(setLoading());
    const response = yield call(getUserPdfFromServer, action.payload);

    yield put(setUserPdf(response));
  } catch (error) {
    yield put(setError(error));
  }
}

export function* watchUserPdf() {
  yield takeEvery(USER_GET_PDF, getUserPdfWorker);
}
