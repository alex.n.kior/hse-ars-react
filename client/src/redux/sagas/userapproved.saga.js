import axios from 'axios';
import { call, put, takeEvery } from 'redux-saga/effects';

import { setError, setLoading, setUserApproved } from '../actions/userapproved.actions';
import { USER_APPROVED } from '../types';

async function setUserApprovedToServer({ userId }) {
    try {
        const response = await axios.patch(`http://localhost:3001/api/updateuser/isapproved`, {
            userId: userId,
        });

        return response.status;
    } catch (error) {
        throw Error(error);
    }
}

function* setUserApprovedWorker(action) {
    try {
        yield put(setLoading());
        const response = yield call(setUserApprovedToServer, action.payload);

        yield put(setUserApproved(response));
    } catch (error) {
        yield put(setError(error));
    }
}

export function* watchUserApproved() {
    yield takeEvery(USER_APPROVED, setUserApprovedWorker);
}
