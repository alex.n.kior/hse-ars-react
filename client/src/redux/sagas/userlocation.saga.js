import axios from 'axios';
import { call, put, takeEvery } from 'redux-saga/effects';

import { setError, setLoading, setUserLocation } from '../actions/userlocation.actions';
import { USER_ADD_LOCATION } from '../types';

async function setUserLocationToServer({ userLocationId, userId }) {
    try {
        const response = await axios.patch(`http://localhost:3001/api/updateuser/location/${userId}`, {
            chosen_location: userLocationId,
        });

        return response.status;
    } catch (error) {
        throw Error(error);
    }
}

function* setUserLocationWorker(action) {
    try {
        yield put(setLoading());
        const response = yield call(setUserLocationToServer, action.payload);

        yield put(setUserLocation(response));
    } catch (error) {
        yield put(setError(error));
    }
}

export function* watchUserLocation() {
    yield takeEvery(USER_ADD_LOCATION, setUserLocationWorker);
}
