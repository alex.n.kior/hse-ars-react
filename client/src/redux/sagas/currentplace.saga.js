import axios from "axios";
import { call, put, takeEvery } from "redux-saga/effects";

import {
  setCurrentPlace,
  setError,
  setLoading,
} from "../actions/currentplace.actions";
import { GET_CURRENT_PLACE } from "../types";

async function getCurrentPlaceFromServer({ placeId }) {
  try {
    const response = await axios.post(
      `http://localhost:3001/api/place/current`,
      {
        placeId: placeId,
      }
    );
    return response.data;
  } catch (error) {
    throw Error(error);
  }
}

function* getCurrentPlaceWorker(action) {
  try {
    yield put(setLoading());
    const response = yield call(getCurrentPlaceFromServer, action.payload);

    yield put(setCurrentPlace(response));
  } catch (error) {
    yield put(setError(error));
  }
}

export function* watchCurrentPlace() {
  yield takeEvery(GET_CURRENT_PLACE, getCurrentPlaceWorker);
}
