import axios from 'axios';
import { call, put, takeEvery } from 'redux-saga/effects';

import { setChosenAddress, setError, setLoading } from '../actions/chosenaddress.actions';
import { GET_ADDRESS } from '../types';

async function getChosenAddressFromServer({addressId}) {
    try {
        const response = await axios.get(`http://localhost:3001/api/address/${addressId}`);
        return response.data;
    } catch (error) {
        throw Error(error);
    }
}

function* getChosenAddressWorker(action) {
    try {
        yield put(setLoading());
        const response = yield call(getChosenAddressFromServer, action.payload);

        yield put(setChosenAddress(response));
    } catch (error) {
        yield put(setError(error));
    }
}

export function* watchChosenAddress() {
    yield takeEvery(GET_ADDRESS, getChosenAddressWorker);
}
