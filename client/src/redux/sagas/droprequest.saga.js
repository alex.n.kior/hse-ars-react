import axios from 'axios';
import { call, put, takeEvery } from 'redux-saga/effects';

import { setError, setLoading, setDropRequest } from '../actions/droprequest.actions';
import { USER_DROP_REQUEST } from '../types';

async function setDropRequestToServer({userId }) {
    try {
        const response = await axios.patch(`http://localhost:3001/api/updateuser/request/drop/${userId}`, {
        });

        return response.request;
    } catch (error) {
        throw Error(error);
    }
}

function* setDropRequestWorker(action) {
    try {
        yield put(setLoading());
        const response = yield call(setDropRequestToServer, action.payload);

        yield put(setDropRequest(response));
    } catch (error) {
        yield put(setError(error));
    }
}

export function* watchDropRequest() {
    yield takeEvery(USER_DROP_REQUEST, setDropRequestWorker);
}
