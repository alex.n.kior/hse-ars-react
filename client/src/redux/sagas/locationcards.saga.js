import axios from 'axios';
import { call, put, takeEvery } from 'redux-saga/effects';

import { setLocationCards, setError, setLoading } from '../actions/locationcards.actions';
import { GET_LOCATION_CARDS } from '../types';

async function getLocationCardsFromServer({addressId}) {
    try {
        const responce = await axios.get(`http://localhost:3001/api/locations/${addressId}`);
        return responce.data;
    } catch (error) {
        throw Error(error);
    }
}

function* getLocationCardsWorker(action) {
    try {
        yield put(setLoading());
        const responce = yield call(getLocationCardsFromServer, action.payload);

        yield put(setLocationCards(responce));
    } catch (error) {
        yield put(setError(error));
    }
}

export function* watchLocationCards() {
    yield takeEvery(GET_LOCATION_CARDS, getLocationCardsWorker);
}
