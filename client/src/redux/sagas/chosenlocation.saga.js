import axios from 'axios';
import { call, put, takeEvery } from 'redux-saga/effects';

import { setChosenLocation, setError, setLoading } from '../actions/chosenlocation.actions';
import { GET_LOCATION } from '../types';

async function getChosenLocationFromServer({locationId}) {
    try {
        const response = await axios.get(`http://localhost:3001/api/location/${locationId}`);
        return response.data;
    } catch (error) {
        throw Error(error);
    }
}

function* getChosenLocationWorker(action) {
    try {
        yield put(setLoading());
        const response = yield call(getChosenLocationFromServer, action.payload);

        yield put(setChosenLocation(response));
    } catch (error) {
        yield put(setError(error));
    }
}

export function* watchChosenLocation() {
    yield takeEvery(GET_LOCATION, getChosenLocationWorker);
}
