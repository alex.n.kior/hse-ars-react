import axios from 'axios';
import { call, put, takeEvery } from 'redux-saga/effects';

import { setError, setLoading, setUserSignUp } from '../actions/usersignup.actions';
import { USER_SIGN_UP } from '../types';

async function setUserSignUpToServer({ signUpForm }) {
    try {
        const response = await axios.post('http://localhost:3001/api/user/signup',{
          data: signUpForm
        });

        return response.data;
    } catch (error) {
        throw Error(error);
    }
}

function* setUserSignUpWorker(action) {
    try {
        yield put(setLoading());
        const response = yield call(setUserSignUpToServer, action.payload);

        yield put(setUserSignUp(response));
    } catch (error) {
        yield put(setError(error));
    }
}

export function* watchUserSignUp() {
    yield takeEvery(USER_SIGN_UP, setUserSignUpWorker);
}
