import React from "react";
import { Footer, IconActionLinkOpen, SidebarVariant } from "@hse-design/react";

function ArsFooter() {
  return (
    <section>
      <div style={{ zoom: 0.76 }}>
        <Footer
          socialLinks={{
            vk: "https://vk.com",
            fb: "https://www.facebook.com",
            vm: "https://vimeo.com",
            inst: "https://instagram.com",
            youtube: "https://youtube.com",
            custom: "https://google.com",
          }}
          socialIcons={{
            custom: IconActionLinkOpen,
          }}
          copyrightText="© Все права защищены, 2021"
          items={[
            {
              title: "Студентам",
              items: [
                {
                  title: "Расписание занятий",
                  href: "#",
                },
                {
                  title: "Портал HSE.ru",
                  href: "#",
                },
                {
                  title: "Личный кабинет",
                  href: "#",
                },
                {
                  title: "Как поступить",
                  href: "#",
                },
              ],
            },
            {
              title: "Преподавателям",
              items: [
                {
                  title: "Расписание занятий",
                  href: "#",
                },
                {
                  title: "Расписание семинаров",
                  href: "#",
                },
                {
                  title: "Личный кабинет",
                  href: "#",
                },
              ],
            },
            {
              title: "Сотрудникам",
              items: [
                {
                  title: "Страница профсоюза",
                  href: "#",
                },
                {
                  title: "Группы в соцсетях",
                  href: "#",
                },
                {
                  title: "Вакансии",
                  href: "#",
                },
              ],
            },
            {
              title: "Общие ресурсы",
              items: [
                {
                  title: "Вопросы и ответы",
                  href: "#",
                },
                {
                  title: "Портал HSE.ru",
                  href: "#",
                },
                {
                  title: "mos.ru",
                  href: "#",
                },
              ],
            },
          ]}
        />
      </div>
    </section>

  );
}

export default ArsFooter;
