import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import LocationCard from '../LocationCard/LocationCard.jsx';
import { getLocationCards } from '../../redux/actions/locationcards.actions';
import { Spinner, Size } from '@hse-design/react/lib/es/components/Spinner';
import { getCurrentUser } from '../../redux/actions/currentuser.actions';

function LocationCards() {
    const dispatch = useDispatch();
    const { locationCardsList, isLoading, error } = useSelector((state) => state.locationCards);

    const { current_user } = useSelector((state) => state.currentUser);

    const addressId = current_user.chosen_address_id;

    useEffect(() => {
        dispatch(getLocationCards({ addressId }));
    }, []);

    return (
        <section className="h-auto w-full flex flex-col justify-start content-evenly pb-32">
            {isLoading && <Spinner indeterminate size={Spinner.Size.XL} />}
            {error ? (
                <Spinner indeterminate size={Spinner.Size.XL} />
            ) : (
                <>
                    {locationCardsList &&
                        locationCardsList.map((item, index) => (
                            <LocationCard key={index.toString()} {...item} />
                        ))}
                </>
            )}
        </section>
    );
}

export default LocationCards;
