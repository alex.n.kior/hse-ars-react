import React, { useState, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { BrowserRouter as Route, Switch, Link, Router } from 'react-router-dom';
import { Button } from '@hse-design/react';
import { Card } from '@hse-design/react/lib/es/components/Card';
import { Spinner, Size } from '@hse-design/react/lib/es/components/Spinner';
import RequestAddressCard from '../../components/RequestAddressCard/RequestAddressCard.jsx';
import RequestLocationCard from '../../components/RequestLocationCard/RequestLocationCard.jsx';
import RequestPlaceCard from '../../components/RequestPlaceCard/RequestPlaceCard.jsx';
import { getCurrentUser } from '../../redux/actions/currentuser.actions';
import { getChosenAddress } from '../../redux/actions/chosenaddress.actions';
import { getChosenLocation } from '../../redux/actions/chosenlocation.actions';
import { getChosenPlace } from '../../redux/actions/chosenplace.actions';

export function RequestCards() {
    const dispatch = useDispatch();
    const { current_user, isLoading, error } = useSelector((state) => state.currentUser);
    const { chosen_address } = useSelector((state) => state.chosenAddress);
    const { chosen_location } = useSelector((state) => state.chosenLocation);
    const { chosen_place } = useSelector((state) => state.chosenPlace);

    const addressId = chosen_address;
    const locationId = chosen_location;
    const placeId = chosen_place;

    useEffect(() => {
      dispatch(getChosenAddress({ addressId }));
      dispatch(getChosenLocation({ locationId }));
      dispatch(getChosenPlace({ placeId }));
    }, []);
    return (
        <>
            <section className="h-auto w-full flex flex-row justify-between pb-2 pt-2">
                {isLoading && <Spinner indeterminate size={Spinner.Size.XL} />}

                {current_user.chosen_address_id ? (
                    <>
                        {chosen_address &&
                            chosen_address.map((item, index) => (
                                <RequestAddressCard key={index.toString()} {...item} />
                            ))}
                    </>
                ) : (<Spinner indeterminate size={Spinner.Size.XL} />)}

                {current_user.chosen_location_id ? (
                    <>
                        {chosen_location &&
                            chosen_location.map((item, index) => (
                                <RequestLocationCard key={index.toString()} {...item} />
                            ))}
                    </>
                ) : (<Spinner indeterminate size={Spinner.Size.XL} />)}

                {current_user.chosen_place_id ? (
                    <>
                        {chosen_place &&
                            chosen_place.map((item, index) => (
                                <RequestPlaceCard key={index.toString()} {...item} />
                            ))}
                    </>
                ) : (<Spinner indeterminate size={Spinner.Size.XL} />)}
            </section>
        </>
    );
}
