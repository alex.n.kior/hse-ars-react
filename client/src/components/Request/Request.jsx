import React, { useState, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { BrowserRouter as Route, Switch, Router } from 'react-router-dom';
import { Link } from '@hse-design/react'
import { getCurrentUser } from '../../redux/actions/currentuser.actions';
import { RequestParams } from '../RequestParams/RequestParams.jsx';
import { RequestCards } from '../RequestCards/RequestCards.jsx';
import { RequestInfo } from '../RequestInfo/RequestInfo.jsx';
import { RequestForm } from '../RequestForm/RequestForm.jsx';

export function Request() {
    const { current_user, isLoading, error } = useSelector((state) => state.currentUser);

    return (
        <>
            <section className="h-auto w-full flex flex-col justify-start content-center pt-8">
                <h2 className="self-center">Спасибо! Вы забронировали вариант размещения.</h2>
                <p className="self-center pt-4">
                    Подтверждение и информация о брони автоматически отправится на ваш Email: <Link href="https://www.hse.ru/" target="_blank">{current_user.email}</Link>.
                </p>
        </section>

            <section className="h-auto w-full flex flex-col justify-start content-center pt-8">
                <RequestParams />
            </section>

            <section className="h-auto w-full flex flex-col justify-start content-center pt-8">
                <RequestCards />
            </section>

            <section className="h-auto w-full flex flex-col justify-start content-center pt-8 pb-16">
                <RequestInfo />
            </section>
        </>
    );
}
