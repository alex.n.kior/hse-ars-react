import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import PlaceCard from '../PlaceCard/PlaceCard.jsx';
import { getPlaceCards } from '../../redux/actions/placecards.actions';
import { Spinner, Size } from '@hse-design/react/lib/es/components/Spinner';
import { getCurrentUser } from '../../redux/actions/currentuser.actions';

function PlaceCards() {
    const dispatch = useDispatch();
    const { placeCardsList, isLoading, error } = useSelector((state) => state.placeCards);

    const { current_user } = useSelector((state) => state.currentUser);

    const locationId = current_user.chosen_location_id;

    useEffect(() => {
        dispatch(getPlaceCards({ locationId }));
    }, []);

    return (
        <section className="h-auto w-full flex flex-col justify-start content-evenly pb-32">
            {isLoading && <Spinner indeterminate size={Spinner.Size.XL} />}
            {error ? (
                <Spinner indeterminate size={Spinner.Size.XL} />
            ) : (
                <>
                    {placeCardsList &&
                        placeCardsList.map((item, index) => (
                            <PlaceCard key={index.toString()} {...item} />
                        ))}
                </>
            )}
        </section>
    );
}

export default PlaceCards;
