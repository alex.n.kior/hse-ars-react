import React, { useState, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { BrowserRouter as Route, Switch, Link, Router } from 'react-router-dom';
import { Button } from '@hse-design/react';
import { Card } from '@hse-design/react/lib/es/components/Card';
import { Divider } from '@hse-design/react';
import { getCurrentUser } from '../../redux/actions/currentuser.actions';
import { getChosenAddress } from '../../redux/actions/chosenaddress.actions';
import { getChosenLocation } from '../../redux/actions/chosenlocation.actions';
import { getChosenPlace } from '../../redux/actions/chosenplace.actions';

export function RequestInfo() {
    const dispatch = useDispatch();

    const { current_user, isLoading, error } = useSelector((state) => state.currentUser);
    const { chosen_address } = useSelector((state) => state.chosenAddress);
    const { chosen_location } = useSelector((state) => state.chosenLocation);
    const { chosen_place } = useSelector((state) => state.chosenPlace);

    const addressId = current_user.chosen_address_id;
    const locationId = current_user.chosen_location_id;
    const placeId = current_user.chosen_place_id;

    useEffect(() => {
        dispatch(getChosenAddress({ addressId }));
        dispatch(getChosenLocation({ locationId }));
        dispatch(getChosenPlace({ placeId }));
    }, []);
    return (
        <>
            <Card className="h-auto w-full flex flex-col pb-8 pt-2">
                <div className="mb-12 flex flex-col justify-start">
                    <h4>Условия договора (не является офертой):</h4>

                    <p className="pt-4">
                        Стоимость проживания в месяц: {chosen_place[0]?.cost_per_month} руб/мес
                    </p>

                    <p className="pt-4">Первоначальный депозит: {chosen_place[0]?.deposit} руб</p>

                    <p className="pt-4">Дата начала заселения: {chosen_place[0]?.starts}</p>

                    <p className="pt-4">Номер договора: не присвоен</p>
                </div>

                <Divider color={Divider.Color.dark} />

                <div className="mt-4 flex flex-col justify-start pb-2">
                    <h4>Арендатор:</h4>

                    <p className="pt-4">ФИО: {current_user?.full_name}</p>

                    <p className="pt-4">Пол: {current_user?.sex}</p>

                    <p className="pt-4">
                        Дата рождения, полных лет: {current_user?.date_of_birth},{' '}
                        {current_user?.age}
                    </p>

                    <p className="pt-4">Гражданство: {current_user?.citizenship}</p>

                    <p className="pt-4">
                        Документы удостоверяющие личность: {current_user?.documents}
                    </p>

                    <p className="pt-4">
                        Место постоянной регистрации: {current_user?.registration}
                    </p>

                    <p className="pt-4">Email: {current_user?.email}</p>

                    <p className="pt-4">Телефон: {current_user?.phone}</p>

                    <p className="pt-4">
                        Арендатор является плательщиком по договору:{' '}
                        {current_user?.is_contract ? 'Да' : 'Нет'}
                    </p>
                </div>
            </Card>
        </>
    );
}
