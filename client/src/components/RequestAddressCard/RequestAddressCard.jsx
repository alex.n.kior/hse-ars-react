import * as React from 'react';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import { useDispatch, useSelector } from 'react-redux';
import Typography from '@mui/material/Typography';
import { Button } from '@hse-design/react';
import { Card } from '@hse-design/react/lib/es/components/Card';

function RequestAddressCard({ id, pic, name, unoccupied, description }) {
    return (
        <>
        <Card className="w-[31%] h-auto border-2 flex flex-col justify-start" id={id}>
        <div className="w-full h-48 overflow-hidden flex flex-col mb-4">
                    <img className="object-contain max-h-48" src={pic} alt=""/>
                </div>

                <CardContent>
                    <Typography gutterBottom variant="h6" component="div">
                        {name}
                    </Typography>
                    <div className="w-full h-16 flex flex-col justify-between content-evenly">
                        <Typography variant="p" color="text.primary">
                            Кол-во свободных мест: <br></br>
                            {unoccupied}
                        </Typography>
                    </div>
                    <div className="w-full h-20 flex flex-col justify-between content-evenly">
                        <Typography variant="p" color="text.primary">
                            Описание: <br></br>
                            {description}
                        </Typography>
                    </div>
                </CardContent>
            </Card>
        </>
    );
}

export default RequestAddressCard;
