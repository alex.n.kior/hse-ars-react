import React, { useState, useEffect } from "react";
import { FormControl, InputLabel } from "@mui/material";
import { useDispatch, useSelector } from "react-redux";
import { Button } from "@hse-design/react";
import { Input } from "@hse-design/react";
import { bindActionCreators, compose } from "redux";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import { setUserSignIn } from "../../redux/actions/usersignin.actions";
import { useHistory } from "react-router-dom";

export function ArsSignIn() {
  const history = useHistory();
  const dispatch = useDispatch();
  const handleRoute = () => {
    history.push("/");
    window.location.reload();
  };

  const [formValue, setformValue] = React.useState({
    email: "",
    password: "",
  });

  const handleSubmit = (event) => {
    event.preventDefault();
    const signInForm = { email: formValue.email, password: formValue.password };

    dispatch(setUserSignIn({ signInForm }));
    localStorage.setItem('isAuth', formValue.email);
  };

  const handleChange = (event) => {
    setformValue({
      ...formValue,
      [event.target.name]: event.target.value,
    });
  };
  
  return (
    <>
      <h3>Авторизация</h3>
      <form onSubmit={handleSubmit}>
        <FormControl margin="normal" required fullWidth>
          <Input
            type="email"
            name="email"
            id="email"
            mask="email"
            value={formValue.email}
            required
            fullWidth
            onChange={handleChange}
            placeholder="Введите e-mail"
          />
        </FormControl>
        <FormControl margin="normal" required fullWidth>
          <Input
            type="password"
            name="password"
            id="password"
            value={formValue.password}
            required
            fullWidth
            onChange={handleChange}
            placeholder="Введите пароль"
          />
        </FormControl>
        {/* {props.errorMessage &&
          <InputLabel error>{props.errorMessage}</InputLabel>
        } */}
        <Button
          className="mt-4 self-center"
          onClick={handleRoute}
          type="submit"
          variant="primary"
          size="medium"
        >
          Войти
        </Button>
      </form>
    </>
  );
}
