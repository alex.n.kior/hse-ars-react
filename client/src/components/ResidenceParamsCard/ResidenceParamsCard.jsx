import * as React from 'react';
import { useHistory } from 'react-router-dom';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import { useDispatch, useSelector } from 'react-redux';
import Typography from '@mui/material/Typography';
import { Button } from '@hse-design/react';
import { Card } from '@hse-design/react/lib/es/components/Card';
import { setUserPlace } from '../../redux/actions/userplace.actions';

function ResidenceParamsCard({
    id,
    pic,
    place_type,
    sex,
    cost_per_month,
    deposit,
    address,
    rooms,
    occupied,
    unoccupied_in_room,
    description,
    bed_type,
    bed_place,
    place_plan,
    floor_plan,
    starts,
}) {

    const { current_user, isLoading, error } = useSelector((state) => state.currentUser);

    return (
        <>
            <div className="w-full h-auto mt-4 flex flex-row justify-between" id={id}>
                <div className="w-128 w-min-56 max-h-60 h-full overflow-hidden flex flex-row justify-start">
                    <img className="object-contain max-w-128 max-h-56 self-start" src={pic} alt="" />
                </div>
                <div className="w-[68%] h-full flex flex-col">

                    <div className="flex flex-row justify-between">
                        <Typography gutterBottom variant="h6" component="div">
                            {address}
                        </Typography>
                        <Button variant="tertiary" size="small">
                            {bed_type}
                        </Button>
                        <Button variant="tertiary" size="small">
                            {bed_place}
                        </Button>
                    </div>
                    <div className="w-full h-12 flex flex-row flex-wrap justify-between content-between pt-4 mb-2">
                        <Button variant="secondary" size="small">
                            {sex}
                        </Button>
                        <Button variant="secondary" size="small">
                            {place_type}
                        </Button>
                        <Button variant="secondary" size="small">
                            План квартиры
                        </Button>
                        <Button variant="secondary" size="small">
                            План этажа
                        </Button>
                    </div>

                    <div className="w-full h-20 flex flex-row justify-between content-evenly pt-4">
                        <Typography className="w-[40%]" variant="p" color="text.primary">
                            Описание: <br></br>
                            {description}
                        </Typography>
                    </div>
                    <div className="w-full h-16 flex flex-row justify-start content-end">
                        <Typography className="w-[50%]" variant="p" color="text.secondary">
                            Всего в квартире: <br></br>
                            комнат {rooms} | свободных мест {unoccupied_in_room} из {occupied}
                        </Typography>
                        <Typography className="w-[50%]" variant="p" color="text.secondary">
                            Всего в комнате: <br></br>
                            свободных мест {unoccupied_in_room} из {unoccupied_in_room}
                        </Typography>
                    </div>
                </div>
            </div>
        </>
    );
}

export default ResidenceParamsCard;
