import * as React from 'react';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import { useDispatch, useSelector } from 'react-redux';
import Typography from '@mui/material/Typography';
import { setUserLocation } from '../../redux/actions/userlocation.actions';
import { Button } from '@hse-design/react';
import { Card } from '@hse-design/react/lib/es/components/Card';

function LocationCard({ id, pic, name, address, unoccupied, price_min, price_max, description }) {
    const dispatch = useDispatch();

    const { current_user, isLoading, error } = useSelector((state) => state.currentUser);

    const userLocationHandler = async (e) => {
        e.preventDefault();
        const userLocationId = id;
        const userId = current_user.id;
        dispatch(setUserLocation({ userLocationId, userId }));
        setTimeout(() => {
            window.location.reload();
        });
    };

    return (
        <>
            <Card className="w-full h-auto border-2 mt-12 flex flex-row justify-between" id={id}>
                <div className="w-64 w-min-64 max-h-40 h-full overflow-hidden flex flex-row justify-start">
                    <img className="object-contain max-w-64 max-h-40 self-start" src={pic} alt="" />
                </div>
                <div className="w-[60%] flex flex-row">
                    <div className="w-[50%] h-24 flex flex-col justify-between">
                        <Typography gutterBottom variant="h5" component="div">
                            {name}
                        </Typography>
                        <Typography gutterBottom variant="p" component="div">
                            Адрес: {address}
                        </Typography>
                    </div>

                    <div className="w-[50%] h-24 flex flex-col justify-between">
                        <Typography variant="p" color="text.primary">
                            Стоимость: от {price_min} до {price_max}
                        </Typography>

                        <Typography variant="p" color="text.primary">
                            Описание: {description}
                        </Typography>

                        <Typography variant="p" color="text.secondary">
                            Свободных мест: {unoccupied}
                        </Typography>
                    </div>
                </div>
                <CardActions>
                    <Button onClick={userLocationHandler} variant="primary" size="medium">
                        Выбрать
                    </Button>
                </CardActions>
            </Card>
        </>
    );
}

export default LocationCard;
