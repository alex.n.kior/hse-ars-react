import * as React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import Box from '@mui/material/Box';
import Stepper from '@mui/material/Stepper';
import Step from '@mui/material/Step';
import StepLabel from '@mui/material/StepLabel';
import Typography from '@mui/material/Typography';
import { Button } from '@hse-design/react';
import { Card } from '../../../node_modules/@hse-design/react/lib/es/components/Card';
import { Space, Size } from '../../../node_modules/@hse-design/react/lib/es/components/Space'

const steps = [{ title: 'Шаг 1: Выберите вариант размещения', pic: '' }, { title: 'Шаг 2: Выберите локацию', pic: ''}, { title: 'Шаг 3: Выберите место', pic: '' }];

function ArsStepper(props) {
  return (
    <section className="h-auto w-[100%] flex-row justify-center pt-8">
      <div>
        <Stepper activeStep={props.step}>
          {steps.map((label, index) => {
            const stepProps = {};
            const labelProps = {};

            return (
              <Step key={label} {...stepProps} sx={{ p: 0 }}>
                <Card className="flex flex-row justify-between m-0">
                  <StepLabel {...labelProps}>{label.title}
                    {
                      label.pic ? (
                        <img src={label.pic} alt={label.pic} />
                      ) : null
                    }
                  </StepLabel>
                </Card>
              </Step>
            );
          })}
        </Stepper>
      </div>
    </section>
  );
}

export default ArsStepper 
