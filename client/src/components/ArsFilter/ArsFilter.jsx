import React, { useState, useEffect, useMemo } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { BrowserRouter as Route, Switch, Link, Router } from 'react-router-dom';
import { Select } from '@hse-design/react';
import { Paragraph } from '@hse-design/react';

export function ArsFilter() {
    const [valueBedType, setValueBedType] = useState({ label: 'Не важно', value: 1 });
    const [valueNeighbours, setValueNeighbours] = useState({
        label: 'Не важно',
        value: 1,
    });
    const [valueFloor, setValueFloor] = useState({ label: 'Не важно', value: 1 });
    const [valueCost, setValueCost] = useState({ label: 'Менее 10 000 р/мес', value: 1 });
    const [valueSort, setValueSort] = useState({ label: 'Сортировать результат', value: 1 });

    const optionsBedType = useMemo(
        () => [
            { label: 'Не важно', value: '1' },
            { label: 'Одноярусная', value: '2' },
            { label: 'Двухъярусная Место: Нижнее', value: '3' },
            { label: 'Двухъярусная Место: Верхнее', value: '4' },
        ],
        [],
    );

    const optionsNeighbours = useMemo(
        () => [
            { label: 'Не важно', value: '1' },
            { label: 'Без соседей в квартире', value: '2' },
            { label: 'Без соседей в комнате', value: '3' },
            { label: 'Возможен 1 сосед в комнате', value: '4' },
            { label: 'Возможен 2 соседа в комнате', value: '5' },
            { label: 'Возможен 2 и более соседа в комнате', value: '6' },
        ],
        [],
    );

    const optionsFloor = useMemo(
        () => [
            { label: 'Не важно', value: '1' },
            { label: '1', value: '2' },
            { label: '2', value: '3' },
            { label: '3', value: '4' },
            { label: '4', value: '5' },
        ],
        [],
    );

    const optionsCost = useMemo(
        () => [
            { label: 'Менее 10 000 р/мес', value: '1' },
            { label: '10 000 - 15 000 р/мес', value: '2' },
            { label: '15 000 - 25 000 р/мес', value: '3' },
            { label: '25 000 - 35 000 р/мес', value: '4' },
            { label: 'Более 35 000 р/мес', value: '5' },
        ],
        [],
    );

    const optionsSort = useMemo(
        () => [
            { label: 'Не важно', value: '1' },
            { label: 'Сначала дешевле', value: '2' },
            { label: 'Сначала дороже', value: '3' },
        ],
        [],
    );
    return (
        <>
            <section className="h-auto w-[98%] flex flex-row justify-between content-evenly self-center pt-8">
                <div className="h-16 flex flex-col justify-between">
                    <label className="pl-3 pb-1 text-sm font-medium" htmlFor="selectbedtype">Тип кровати</label>
                    <Select
                        name="selectbedtype"
                        value={valueBedType}
                        width={190}
                        onChangeOption={setValueBedType}
                        searchable={Select.Searchable.inline}
                        getOptions={(searchValue) => {
                            return new Promise((resolve) => {
                                if (searchValue) {
                                    setTimeout(
                                        () =>
                                            resolve(
                                                optionsBedType.filter((opt) =>
                                                    opt.label.includes(searchValue),
                                                ),
                                            ),
                                        500,
                                    );
                                } else {
                                    resolve(optionsBedType);
                                }
                            });
                        }}
                    />
                </div>

                <div className="h-16 flex flex-col justify-between">
                    <label className="pl-3 pb-1 text-sm font-medium" htmlFor="selectneighbours">Количество соседей</label>
                    <Select
                        name="selectneighbours"
                        value={valueNeighbours}
                        width={190}
                        onChangeOption={setValueNeighbours}
                        searchable={Select.Searchable.inline}
                        getOptions={(searchValue) => {
                            return new Promise((resolve) => {
                                if (searchValue) {
                                    setTimeout(
                                        () =>
                                            resolve(
                                                optionsNeighbours.filter((opt) =>
                                                    opt.label.includes(searchValue),
                                                ),
                                            ),
                                        500,
                                    );
                                } else {
                                    resolve(optionsNeighbours);
                                }
                            });
                        }}
                    />
                </div>

                <div className="h-16 flex flex-col justify-between">
                    <label className="pl-3 pb-1 text-sm font-medium" htmlFor="selectfloor">Этаж</label>
                    <Select
                        name="selectfloor"
                        value={valueFloor}
                        width={190}
                        onChangeOption={setValueFloor}
                        searchable={Select.Searchable.inline}
                        getOptions={(searchValue) => {
                            return new Promise((resolve) => {
                                if (searchValue) {
                                    setTimeout(
                                        () =>
                                            resolve(
                                                optionsFloor.filter((opt) =>
                                                    opt.label.includes(searchValue),
                                                ),
                                            ),
                                        500,
                                    );
                                } else {
                                    resolve(optionsFloor);
                                }
                            });
                        }}
                    />
                </div>

                <div className="h-16 flex flex-col justify-between">
                    <label className="pl-3 pb-1 text-sm font-medium" htmlFor="selectcost">Стоимость</label>
                    <Select
                        name="selectcost"
                        value={valueCost}
                        width={190}
                        onChangeOption={setValueCost}
                        searchable={Select.Searchable.inline}
                        getOptions={(searchValue) => {
                            return new Promise((resolve) => {
                                if (searchValue) {
                                    setTimeout(
                                        () =>
                                            resolve(
                                                optionsCost.filter((opt) =>
                                                    opt.label.includes(searchValue),
                                                ),
                                            ),
                                        500,
                                    );
                                } else {
                                    resolve(optionsCost);
                                }
                            });
                        }}
                    />
                </div>

                <div className="h-16 flex flex-col justify-between">
                    <label className="pl-3 pb-1 text-sm font-medium" htmlFor="selectsort">Сортировка</label>
                    <Select
                        name="selectsort"
                        value={valueSort}
                        width={190}
                        onChangeOption={setValueSort}
                        searchable={Select.Searchable.inline}
                        getOptions={(searchValue) => {
                            return new Promise((resolve) => {
                                if (searchValue) {
                                    setTimeout(
                                        () =>
                                            resolve(
                                                optionsSort.filter((opt) =>
                                                    opt.label.includes(searchValue),
                                                ),
                                            ),
                                        500,
                                    );
                                } else {
                                    resolve(optionsSort);
                                }
                            });
                        }}
                    />
                </div>
            </section>
        </>
    );
}
