import React, { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { BrowserRouter as Route, Switch, Router } from "react-router-dom";
import { Button, Link } from "@hse-design/react";
import { Card } from "@hse-design/react/lib/es/components/Card";
import { Spinner, Size } from "@hse-design/react/lib/es/components/Spinner";
import { Divider } from "@hse-design/react";
import { getCurrentPlace } from "../../redux/actions/currentplace.actions";
import ResidenceParamsCard from "../ResidenceParamsCard/ResidenceParamsCard.jsx";
import { setDropRequest } from '../../redux/actions/droprequest.actions';
import { useHistory } from "react-router-dom";

export function ResidenceParams() {
  const dispatch = useDispatch();
  const { current_user, isLoading, error } = useSelector(
    (state) => state.currentUser
  );

  const { current_place } = useSelector((state) => state.currentPlace);

  const userDisapproveHandler = async (e) => {
    e.preventDefault();
    const userId = current_user.id;
    dispatch(setDropRequest({ userId }));
    setTimeout(() => {
        window.location.reload();
    }, 50);
};
  
  useEffect(() => {
    setTimeout(() => {
      const placeId = current_user.current_place_id;
      dispatch(getCurrentPlace({ placeId }));
    });
  }, [current_user?.current_place_id]);

  return (
    <>
      <Card className="h-auto w-full flex flex-col">
        <div className="flex flex-row justify-between content-center pb-6">
          <h4>Текущий вариант размещения</h4>
          <Button variant="primary" size="small">
            Задолженность
          </Button>

          <Button onClick={userDisapproveHandler} variant="primary" size="small">
            Получить квитанцию
          </Button>

          <Button variant="primary" size="small">
            Оплатить на сайте
          </Button>
        </div>
        <Divider color={Divider.Color.dark} />
        <div className="flex flex-row justify-center pt-4">
          {current_user ? (
            <>
              {current_place &&
                current_place.map((item, index) => (
                  <ResidenceParamsCard key={index.toString()} {...item} />
                ))}
            </>
          ) : (
            <Spinner indeterminate size={Spinner.Size.XL} />
          )}
        </div>
      </Card>
    </>
  );
}
