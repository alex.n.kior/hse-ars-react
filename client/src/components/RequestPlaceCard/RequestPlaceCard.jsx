import * as React from 'react';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import { useDispatch, useSelector } from 'react-redux';
import Typography from '@mui/material/Typography';
import { Button } from '@hse-design/react';
import { Card } from '@hse-design/react/lib/es/components/Card';

function RequestPlaceCard({
    id,
    pic,
    place_type,
    sex,
    cost_per_month,
    deposit,
    address,
    rooms,
    occupied,
    unoccupied_in_room,
    description,
    bed_type,
    bed_place,
    place_plan,
    floor_plan,
    starts,
}) {
    return (
        <>
            <Card className="w-[31%] h-auto border-2 flex flex-col justify-start" id={id}>
                <div className="w-full h-48 overflow-hidden flex flex-col mb-4">
                    <img className="object-contain max-h-48" src={pic} alt="" />
                </div>
                <CardContent>
                    <Typography gutterBottom variant="h6" component="div">
                        {address}
                    </Typography>
                    <div className="w-full h-20 flex flex-col justify-between content-evenly">
                        <Typography variant="p" color="text.primary">
                            Описание: <br></br>
                            {description}
                        </Typography>
                    </div>
                    <div className="w-full h-20 flex flex-row flex-wrap justify-between content-between">
                        <Button variant="secondary" size="small">
                            {sex}
                        </Button>
                        <Button variant="secondary" size="small">
                            {place_type}
                        </Button>
                        <Button variant="secondary" size="small">
                            {bed_type}
                        </Button>
                        <Button variant="secondary" size="small">
                            {bed_place}
                        </Button>
                    </div>
                </CardContent>
                <CardContent>
                    <div className="w-full h-20 flex flex-col justify-between content-between">
                        <Button variant="primary" size="small">
                            План помещения
                        </Button>
                        <Button variant="primary" size="small">
                            План этажа
                        </Button>
                    </div>
                    <div className="w-full h-28 flex flex-col justify-between content-between mt-6">
                        <Typography variant="p" color="text.secondary">
                            Всего: <br></br>
                            комнат {rooms} | свободных мест {unoccupied_in_room} из {occupied}
                        </Typography>
                        <Typography variant="p" color="text.secondary">
                            Всего в комнате: свободных мест {unoccupied_in_room} из{' '}
                            {unoccupied_in_room}
                        </Typography>
                    </div>
                </CardContent>
            </Card>
        </>
    );
}

export default RequestPlaceCard;
