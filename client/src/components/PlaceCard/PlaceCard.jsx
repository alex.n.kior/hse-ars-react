import * as React from "react";
import { useHistory } from "react-router-dom";
import CardActions from "@mui/material/CardActions";
import CardContent from "@mui/material/CardContent";
import { useDispatch, useSelector } from "react-redux";
import Typography from "@mui/material/Typography";
import { Button } from "@hse-design/react";
import { Card } from "@hse-design/react/lib/es/components/Card";
import { setUserPlace } from "../../redux/actions/userplace.actions";

function PlaceCard({
  id,
  pic,
  place_type,
  sex,
  cost_per_month,
  deposit,
  address,
  rooms,
  occupied,
  unoccupied_in_room,
  description,
  bed_type,
  bed_place,
  place_plan,
  floor_plan,
  starts,
}) {
  const dispatch = useDispatch();

  const history = useHistory();

  const { current_user, isLoading, error } = useSelector(
    (state) => state.currentUser
  );

  const userPlaceHandler = async (e) => {
    e.preventDefault();
    const userPlaceId = id;
    const userId = current_user.id;
    setTimeout(() => {
      window.location.reload();
    });
    dispatch(setUserPlace({ userPlaceId, userId }));
  };

  return (
    <>
      <Card
        className="w-full h-auto border-2 mt-12 flex flex-row justify-between"
        id={id}
      >
        <div className="w-64 w-min-64 max-h-40 h-full overflow-hidden flex flex-row justify-start">
          <img
            className="object-contain max-w-64 max-h-40 self-start"
            src={pic}
            alt=""
          />
        </div>
        <div className="w-[60%] h-full flex flex-col pr-4">
          <div className="flex flex-row justify-between">
            <Typography gutterBottom variant="h6" component="div">
              {address}
            </Typography>
            <Button variant="tertiary" size="small">
              {bed_type}
            </Button>
            <Button variant="tertiary" size="small">
              {bed_place}
            </Button>
          </div>
          <div className="w-full h-12 flex flex-row flex-wrap justify-between content-between pt-4 mb-2">
            <Button variant="secondary" size="small">
              {sex}
            </Button>
            <Button variant="secondary" size="small">
              {place_type}
            </Button>
            <Button variant="secondary" size="small">
              План квартиры
            </Button>
            <Button variant="secondary" size="small">
              План этажа
            </Button>
          </div>

          <div className="w-full h-20 flex flex-row justify-between content-evenly pt-4">
            <Typography className="w-[40%]" variant="p" color="text.primary">
              Описание: <br></br>
              {description}
            </Typography>
          </div>
          <div className="w-full h-16 flex flex-row justify-start content-end">
            <Typography className="w-[50%]" variant="p" color="text.secondary">
              Всего в квартире: <br></br>
              комнат {rooms} | свободных мест {unoccupied_in_room} из {occupied}
            </Typography>
            <Typography className="w-[50%]" variant="p" color="text.secondary">
              Всего в комнате: <br></br>
              свободных мест {unoccupied_in_room} из {unoccupied_in_room}
            </Typography>
          </div>
        </div>
        <div className="w-[16%] flex flex-col justify-between content-between">
          <Typography variant="p" color="text.primary">
            Начало заселения: <br></br>
            {starts}
          </Typography>

          <Typography variant="p" color="text.secondary">
            Стоимость в месяц: <br></br>
            {cost_per_month}
          </Typography>

          <Typography variant="p" color="text.secondary">
            Депозит: <br></br>
            {deposit}
          </Typography>

          <Button onClick={userPlaceHandler} variant="primary" size="medium">
            Забронировать
          </Button>
        </div>
        {/* {place_type}
                        {sex}
                        {cost_per_month}
                        {deposit}
                        {rooms}
                        {occupied}
                        {unoccupied_in_room}
                        {description}
                        {bed_type}
                        {bed_place}
                        {place_plan}
                        {floor_plan}
                        {starts} */}
      </Card>
    </>
  );
}

export default PlaceCard;
