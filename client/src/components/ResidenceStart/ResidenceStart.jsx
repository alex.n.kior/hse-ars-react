import React, { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { BrowserRouter as Route, Switch, Router } from "react-router-dom";
import { Button, Link, IconActionDownload } from "@hse-design/react";
import { Card } from "@hse-design/react/lib/es/components/Card";
import { Spinner, Size } from "@hse-design/react/lib/es/components/Spinner";
import { Divider } from "@hse-design/react";
import { getCurrentPlace } from "../../redux/actions/currentplace.actions";
import ResidenceParamsCard from "../ResidenceParamsCard/ResidenceParamsCard.jsx";
import { useHistory } from "react-router-dom";
import { setUserApproved } from "../../redux/actions/userapproved.actions";
import { getUserPdf } from "../../redux/actions/userpdf.actions";
import Box from "@mui/material/Box";
import Modal from "@mui/material/Modal";
import NewWindow from "rc-new-window";
import { PDFViewer } from "@react-pdf/renderer";
import { ResidenceStartDocument } from "../ResidenceStartDocument/ResidenceStartDocument.jsx";

export function ResidenceStart() {
  const dispatch = useDispatch();
  const { current_user, isLoading, error } = useSelector(
    (state) => state.currentUser
  );

  const [open, setOpen] = useState(false);

  const userApprovedHandler = (e) => {
    e.preventDefault();
    const userId = current_user.id;

    dispatch(setUserApproved({ userId }));
    setTimeout(() => {
      window.location.reload();
      setOpen((v) => !v);
    }, 50);
  };

  return (
    <>
      <Card className="w-[40%] h-48 border-2 mt-10 flex flex-col justify-between self-center">
        <p>
          {current_user.full_name}, вам предоставлено место в общежитии,
          предоставьте распечатанное направление в общежитие при заселении:
        </p>

        <Button
          onClick={userApprovedHandler}
          className="self-center"
          variant="primary"
          size="medium"
          rightIcon={IconActionDownload}
        >
          Сохранить
        </Button>
      </Card>

      {open ? (
        <NewWindow onClose={() => setOpen(false)}>
          <PDFViewer
            showToolbar={false}
            style={{
              width: "100%",
              height: "95%",
            }}
          >
            <ResidenceStartDocument />
          </PDFViewer>
        </NewWindow>
      ) : null}
    </>
  );
}
