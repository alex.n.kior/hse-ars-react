import * as React from 'react';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import { useDispatch, useSelector } from 'react-redux';
import Typography from '@mui/material/Typography';
import { setUserAddress } from '../../redux/actions/useraddress.actions';
import { Button } from '@hse-design/react';
import { Card } from '@hse-design/react/lib/es/components/Card';

function AddressCard({ id, pic, name, unoccupied, description }) {
    const dispatch = useDispatch();

    const { current_user, isLoading, error } = useSelector((state) => state.currentUser);

    const userAddressHandler = async (e) => {
        e.preventDefault();
        const userAddressId = id;
        const userId = current_user.id;
        dispatch(setUserAddress({ userAddressId, userId }));
        setTimeout(() => {
            window.location.reload();
        });
    };

    return (
        <>
            <Card className="w-full h-auto border-2 mt-12 flex flex-row justify-between" id={id}>
                <div className="w-64 w-min-64 max-h-40 h-full overflow-hidden flex flex-row justify-start">
                    <img className="object-contain max-w-64 max-h-40 self-start" src={pic} alt="" />
                </div>
                <div className="w-[60%]">
                    <Typography gutterBottom variant="h5" component="div">
                        {name}
                    </Typography>
                    <Typography gutterBottom variant="p" component="div">
                        Кол-во свободных мест: <br></br>
                        {unoccupied}
                    </Typography>
                    <Typography variant="body2" color="text.secondary">
                        Описание: <br></br>
                        {description}
                    </Typography>
                </div>
                <CardActions>
                    <Button onClick={userAddressHandler} variant="primary" size="medium">
                        Выбрать
                    </Button>
                </CardActions>
            </Card>
        </>
    );
}

export default AddressCard;
