import React, { useState, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { BrowserRouter as Route, Switch, Link, Router } from 'react-router-dom';
import { Button } from '@hse-design/react';
import { Card } from '@hse-design/react/lib/es/components/Card';
import { getCurrentUser } from '../../redux/actions/currentuser.actions';
import { setDropRequest } from '../../redux/actions/droprequest.actions';
import { useHistory } from "react-router-dom";

export function RequestParams() {
    const dispatch = useDispatch();

    const history = useHistory();

    const { current_user, isLoading, error } = useSelector((state) => state.currentUser);

    const userPlaceHandler = async (e) => {
        e.preventDefault();
        const userId = current_user.id;
        dispatch(setDropRequest({ userId }));
        history.push('/Booking');
        setTimeout(() => {
            window.location.reload();
        });
    };

    return (
        <>
            <Card className="h-auto w-full flex flex-col pt-8">
                <div className="flex flex-row justify-between">
                    <h4>
                        Параметры бронирования от{' '}
                        {new Date().toJSON().slice(0, 10).split('-').reverse().join('.')}
                    </h4>

                    <h4>ID Ars{new Date().toJSON().slice(0, 10).replace(/-/g, '')}</h4>
                </div>

                <div className="flex flex-row justify-between mt-8">
                    <p>
                        Статус: зарезервировано{' '}
                        {new Date().toJSON().slice(0, 10).split('-').reverse().join('.')}
                    </p>

                    <Button variant="secondary" size="small">
                        Добавить комментарий
                    </Button>

                    <Button variant="secondary" size="small">
                        Загрузить документ
                    </Button>

                    <Button onClick={userPlaceHandler} variant="secondary" size="small">
                        Отменить бронирование
                    </Button>
                </div>

                <div className="flex flex-row justify-center mt-8">
                    <p>
                        Оставайтесь на связи. В ближайшее время менеджер свяжется с вами по телефону
                        или Email для подтверждения брони, подписания договора и выставления счета.
                    </p>
                </div>
            </Card>
        </>
    );
}
