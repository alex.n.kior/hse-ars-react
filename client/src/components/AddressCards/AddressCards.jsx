import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import AddressCard from '../AddressCard/AddressCard.jsx';
import { getAddressCards } from '../../redux/actions/addresscards.actions'
import { Spinner, Size } from '@hse-design/react/lib/es/components/Spinner'
import { getCurrentUser } from '../../redux/actions/currentuser.actions';

function AddressCards() {
  const dispatch = useDispatch();
  const { addressCardsList, isLoading, error } = useSelector(
    (state) => state.addressCards
  )

  useEffect(() => {
    dispatch(getAddressCards())
  }, [])

  return (
    <section className="h-auto w-full flex flex-col justify-start content-evenly pb-32">
      {
        isLoading && (
          <Spinner indeterminate size={Spinner.Size.XL} />
        )
      }
      {
        error ? (
          <Spinner indeterminate size={Spinner.Size.XL} />
        ) : (
            <>
              {addressCardsList
                && addressCardsList.map((item, index) => (
                  <AddressCard key={index.toString()} {...item} />
                ))}
            </>
          )
      }
    </section>
  );
}

export default AddressCards
