import React, { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { A } from "hookrouter";

import { Link, useHistory, useLocation } from "react-router-dom";

import {
  Button,
  Header,
  HeaderHamburger,
  HeaderMenu,
  HeaderLink,
  HeaderLanguageSelect,
  HeaderDivider,
  HeaderIcon,
  HeaderUserAvatar,
  IconGlobalMessage,
  IconActionLogout,
  LogoDigital,
  LogoSmartLms,
} from "@hse-design/react";

import {
  Space,
  Size,
} from "../../../node_modules/@hse-design/react/lib/es/components/Space";
import { getCurrentUser } from "../../redux/actions/currentuser.actions";

function ArsHeader() {
  const { current_user, isLoading, error } = useSelector(
    (state) => state.currentUser
  );


  const handleHref = () => {
    window.location.reload();
  };
  const history = useHistory();
  const location = useLocation();

  const handleLogout = () => {
    localStorage.clear();
    window.location.replace("/");
  };

  function setClicked(currentLocation) {
    if (location.pathname == currentLocation) {
      return "primary";
    } else {
      return "tertiary";
    }
  }

  const userSignIn = useSelector((state) => state.userSignIn);
  const [currentLanguage, setCurrentLanguage] = useState("ru");
  const [title] = useState("Личный кабинет проживающего");
  const userImg =
    "https://upload.wikimedia.org/wikipedia/commons/7/7c/Profile_avatar_placeholder_large.png";
  const languages = [
    {
      label: "RU",
      value: "ru",
    },
    {
      label: "EN",
      value: "en",
    },
  ];
  const menu = [
    {
      name: {
        ru: "Личный кабинет проживающего",
        en: "Resident office",
      },
      page: "Applications",
    },
  ];

  return (
    <>
      <Header
        prepend={
          <div
            className={"tablet-small-down-show"}
            style={{ display: "flex", alignItems: "center" }}
          >
            <HeaderHamburger />
          </div>
        }
        logo={<LogoDigital style={{ height: "48px" }} />}
        left={
          localStorage.getItem("isAuth") ? (
            <HeaderMenu>
              {current_user?.is_approved || !current_user?.has_accomodation
                ? "Личный кабинет проживающего"
                : "Личный кабинет абитуриента"}
              <Space size={Size["small_2x"]} horizontal />
              <>
                <Space size={Size["small_2x"]} horizontal />
                <A href="/" onClick={handleHref}>
                  <Button variant={setClicked("/")}>Проживание</Button>
                </A>
              </>
              {current_user?.has_accomodation ? null : (
                <>
                  <Space size={Size["small_2x"]} horizontal />
                  <A href="/Booking" onClick={handleHref}>
                    <Button variant={setClicked("/Booking")}>
                      Бронирование
                    </Button>
                  </A>
                </>
              )}

              <>
                <Space size={Size["small_2x"]} horizontal />
                <A href="/Help" onClick={handleHref}>
                  <Button variant={setClicked("/Help")}>Консультация</Button>
                </A>
              </>
            </HeaderMenu>
          ) : (
            <>{title}</>
          )
        }
        right={
          <>
            <div
              className="tablet-small-down-hide"
              style={{ display: "flex", alignItems: "center" }}
            >
              <HeaderLanguageSelect
                languages={languages}
                currentLanguage={currentLanguage}
                onChange={setCurrentLanguage}
              />
              <Space
                className="tablet-small-only-show"
                horizontal
                size={Space.Size.small_3x}
              />
            </div>
            <HeaderIcon icon={IconGlobalMessage} />
            <HeaderDivider className="tablet-small-down-hide" />
            {localStorage.getItem("isAuth") ? (
              <>
                <Space size={Size["small_2x"]} horizontal />
                <HeaderUserAvatar userImg={userImg} />
                <HeaderIcon
                  onClick={handleLogout}
                  className="tablet-small-down-hide rotate-180"
                  icon={IconActionLogout}
                />
              </>
            ) : (
              <>
                {/* <HeaderMenu className="tablet-small-down-hide">
                    <HeaderLink
                    >Войти</HeaderLink>
                    <HeaderLink>Регистрация</HeaderLink>
                  </HeaderMenu> */}
              </>
            )}
            <>
              {/* <HeaderDivider className="table-small-down-show" /> */}
              <HeaderHamburger />
            </>
          </>
        }
      ></Header>
    </>
  );
}

export default ArsHeader;
